var io = require("socket.io")(process.env.PORT || 52300);
//Custom classes
var Player = require("./Classes/Player");

var players = [];
var socketes = [];

console.log("Server has started");

io.on("connection", function (socket) {
  var player = new Player();
  var thisPlayerID = player.id;

  players[thisPlayerID] = player;
  socket[thisPlayerID] = socket;

  //Tell the clients this my id
  socket.emit("register", { id: thisPlayerID });
  socket.emit("spawn", player); // Tell client it has spawn
  socket.broadcast.emit("spawn", player); // Tell to all clients it has spawn

  //Tell client to all other clients
  for (var playerID in players) {
    if (playerID != thisPlayerID) {
      socket.emit("spawn", players[playerID]);
    }
    socket.on("updatePosition", function (data) {
      player.position.x = data.position.x;
      player.position.y = data.position.y;
      player.position.z = data.position.z;

      socket.broadcast.emit("updatePosition", player);
    });

    console.log("Client is Connected : " + playerID);
    console.log(player.position.ConsoleOutput());
  }
  socket.on("disconnect", function () {
    delete players[thisPlayerID];
    delete socket[thisPlayerID];
    socket.broadcast.emit("disconnected", player);
    console.log("Client is Disconnected : " + thisPlayerID);
  });
});
