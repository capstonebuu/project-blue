module.exports = class Vector3 {
  constructor(_x = 0, _y = 0, _z = 0) {
    this.x = _x;
    this.y = _y;
    this.z = _z;
  }

  Magnitude() {
    return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
  }

  Normalized() {
    var mag = this.Magnitude();
    return new Vector3(this.x / mag, this.y / mag, this.z / mag);
  }

  Distance(OtherVect = Vector3) {
    var direction = new Vector3();
    direction.x = OtherVect.x - this.x;
    direction.y = OtherVect.y - this.y;
    direction.z = OtherVect.z - this.z;
    return direction.Magnitude();
  }

  ConsoleOutput() {
    return "(" + this.x + " , " + this.y + " , " + this.z + ")";
  }
};
