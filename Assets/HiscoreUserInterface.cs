﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
public class HiscoreUserInterface : NetworkBehaviour
{
    [SyncVar(hook = "OnChangeScore")]
    public int playerScore;

    private void Update()
    {
        
    }

    public void OnChangeScore(int oldValue, int newValue)
    {
        playerScore = newValue;
    }


}
