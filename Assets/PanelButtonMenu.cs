﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
public class PanelButtonMenu : MonoBehaviour
{
    [SerializeField] NetworkRoomManager network;

    [Header("User Interface")]
    [SerializeField] TMP_Text titleText;
    public void StartGame()
    {
        StartMode(1);
    }
    public void DEBUG_START_HOST()
    {
        StartMode(2);
    }
    public void DEBUG_START_SERVER()
    {
        StartMode(3);
    }

    public void StartMode(int mode)
    {
        network.networkAddress = "localhost";
        gameObject.SetActive(false);
        titleText.gameObject.SetActive(false);
        switch (mode)
        {
            case 1:
                network.StartClient();
                break;
            case 2:
                network.StartHost();
                break;
            case 3:
                network.StartServer();
                break;
        }
    }
}
