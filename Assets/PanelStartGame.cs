﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Mirror;
public class PanelStartGame : MonoBehaviour
{
    [SerializeField] private NetworkRoomManager networkManager = null;

    [Header("Network IP Address")]
    public string networkipAddress = "localhost";

    [Header("Touch")]
    [SerializeField] private TMP_Text touchText = null;
    [SerializeField] private TMP_Text titleText = null;
    [SerializeField] private Animator animCam;
    private bool previousTouch = false;
    private void Update()
    {
        bool isTouch = Input.touchCount > 0;
        if (isTouch && !previousTouch && touchText.enabled)
        {
            touchText.enabled = false;
            titleText.enabled = false;
            animCam.SetBool("isTouchToStart", true);
        }
        previousTouch = isTouch;
        if (networkManager == null)
        {
            networkManager = GameObject.FindWithTag("NetworkManager").GetComponent<NetworkRoomManager>();
        }
    }
}
