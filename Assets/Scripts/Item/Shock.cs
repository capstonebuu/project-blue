﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Shock : NetworkBehaviour
{
    private bool isPlayerEnter = false;
    public float time = 5f;
    public bool isTrapDestroy { get; set; }
    private GameObject player;
    [SerializeField]
    public GameObject obj;

    // Update is called once per frame



    public override void OnStartServer()
    {
        Invoke(nameof(DestroySelf), time);
        
    }


  
    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player" )
        {
            Debug.Log("Hit");
            other.GetComponent<BasicMovement>().isInteract = true;
            other.GetComponent<BasicMovement>().isStun = true;
            other.GetComponent<BasicMovement>().isCooldownStun = true;
        }
    }



    #region server
    [Server]
    private void DestroySelf()
    {
        NetworkServer.Destroy(gameObject);
    }

    #endregion


}
