﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Block : NetworkBehaviour
{
    public override void OnStartServer()
    {
        Invoke(nameof(DestroySelf), 5);
    }

    #region Server 
    [Server]
    private void DestroySelf()
    {
        NetworkServer.Destroy(gameObject);
    }
    #endregion
}
