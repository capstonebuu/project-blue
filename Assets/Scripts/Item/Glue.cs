﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Glue : NetworkBehaviour
{
    private bool isPlayerEnter = false;
    public float time = 5f;
    public bool isTrapDestroy { get; set; }
    private GameObject player;

    // Update is called once per frame



    public override void OnStartServer()
    {
        StartCoroutine(TrapDestroyed());
        Invoke(nameof(DestroySelf), time);
        

    }

 

    private IEnumerator TrapDestroyed()
    {
        yield return new WaitForSeconds(4.8f);
        UpdateSpeed(player, 5f, Color.white);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isPlayerEnter = true;
            player = other.gameObject;
            UpdateSpeed(other.gameObject, 3f, Color.yellow);
        }


    }



    private void OnTriggerExit(Collider other)
    {
        isPlayerEnter = false;
        //Increase Speed after Exit
        UpdateSpeed(other.gameObject, 5f, Color.white);
        player = null;
    }


    private void UpdateSpeed(GameObject target, float speedValue, Color colorValue)
    {
        if (isServer)
        {
            RpcChangeSpeed(target, speedValue, colorValue);
        }
        else
        {
            CmdChangeSpeed(target, speedValue, colorValue);
        }
    }




    #region server
    [Server]
    private void DestroySelf()
    {
        NetworkServer.Destroy(gameObject);
    }

    [ClientRpc]
    public void RpcDestroy(float time)
    {
        Debug.Log("destroy" + time);
        Destroy(gameObject, time);
    }

    [ClientRpc]
    public void RpcChangeSpeed(GameObject target, float value, Color colorValue)
    {
        if (target != null)
        {
            target.GetComponent<BasicMovement>().speed = value;
            GetComponent<Renderer>().material.color = colorValue;
        }
    }

    [ClientRpc]
    public void RpcActiveEffectSlow()
    {

    }

    [ClientRpc]
    public void RpcDeactiveEffectSlow()
    {

    }
    #endregion


    #region client
    [Command(ignoreAuthority = true)]
    private void CmdDestroy(float time)
    {
        RpcDestroy(time);
    }

    [Command(ignoreAuthority = true)]
    public void CmdChangeSpeed(GameObject target, float value, Color colorValue)
    {
        Debug.Log("target" + target);
        if (target != null)
        {
            Debug.Log("Cmd");
            RpcChangeSpeed(target, value, colorValue);
        }
    }

    [Command(ignoreAuthority = true)]
    void CmdActiveEffectSlow()
    {
        RpcActiveEffectSlow();
    }

    [Command(ignoreAuthority = true)]
    void CmdDeactiveEffectSlow()
    {
        RpcDeactiveEffectSlow();
    }
    #endregion

}
