﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Mirror;
using UnityEngine.UI;
public class MainMenu : MonoBehaviour
{
    [SerializeField] private NetworkRoomManager networkManager = null;

    [Header("Network IP Address")]
    public string networkipAddress = "localhost";

    [Header("Touch")]
    [SerializeField] private TMP_Text touchText = null;
    [SerializeField] private TMP_Text titleText = null;
    [SerializeField] private Animator animCam;
    [SerializeField] private GameObject settingButton;
    [SerializeField] private GameObject infoButton;
    [SerializeField] private Button startGameButton;
    [SerializeField] private GameObject DEBUG_STARTGAME;
    private bool previousTouch = false;


    private void Update()
    {
        if (networkManager == null)
        {
            networkManager = GameObject.FindWithTag("NetworkManager").GetComponent<NetworkRoomManager>();
        }
        if (kcp2k.KcpConnection.Kcpdisconnected)
        {
            animCam.SetBool("isTouchToStart", false);
        }
    }

    public void StartGame()
    {
        touchText.enabled = false;
        titleText.enabled = false;
        infoButton.SetActive(false);
        settingButton.SetActive(false);
        DEBUG_STARTGAME.SetActive(false);
        startGameButton.gameObject.SetActive(false);
        animCam.SetBool("isTouchToStart", true);
    }

    public void StartHost()
    {
        touchText.enabled = false;
        titleText.enabled = false;
        infoButton.SetActive(false);
        settingButton.SetActive(false);
        DEBUG_STARTGAME.SetActive(false);
        startGameButton.gameObject.SetActive(false);
        networkManager.StartHost();
    }
    public void BackToMenu()
    {
        touchText.enabled = true;
        titleText.enabled = true;
        //DEBUG_STARTGAME.SetActive(true);
        startGameButton.gameObject.SetActive(true);
        infoButton.SetActive(true);
        //settingButton.SetActive(true);
    }
}
