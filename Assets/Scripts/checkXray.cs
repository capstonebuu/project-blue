﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkXray : MonoBehaviour
{
    private Material cube;
    private float alphaValue = 1;
    // Start is called before the first frame update
    void Start()
    {
        cube = this.transform.parent.GetComponent<Renderer>().material;
        if(cube == null)
        {
            Debug.Log("NULL");
        }
    }

    // Update is called once per frame
    void Update()
    {
        TransObject(cube, alphaValue);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Debug.Log("Enter");
            alphaValue = 0.2f;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            Debug.Log("Exit");
            alphaValue = 1.0f;
        }
    }
    void TransObject(Material mat,float alpha)
    {
        Color currecntColor = mat.color;
        Color newColor = new Color(currecntColor.r, currecntColor.g, currecntColor.b, alpha);
        mat.SetColor("_Color", newColor);
    }
}
