﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class interactable_slowActive : NetworkBehaviour
{
    [SerializeField] public Joybutton_Interact interactButton;

    private void Update()
    {
        if (interactButton == null)
        {
            interactButton = GameObject.FindWithTag("InteractButton").GetComponent<Joybutton_Interact>();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (interactable_slowPad.isOilActive == false && interactButton != null)
        {
            if (other.tag == "Player")
            {
                interactButton.interactable = true;

                if ((Input.GetKeyDown(KeyCode.E) || interactButton.Pressed))
                {
                    interactable_slowPad.isOilActive = true;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && interactButton != null)
        {
            interactButton.interactable = false;
        }
    }


}
