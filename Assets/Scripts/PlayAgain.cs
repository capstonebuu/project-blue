﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;
public class PlayAgain : MonoBehaviour
{
    [SerializeField] NetworkRoomManager managerExt;
    private void Update()
    {
        if (managerExt == null)
        {
            Debug.Log("Nulll");
            managerExt = GameObject.FindWithTag("NetworkManager").GetComponent<NetworkRoomManager>();
        }
    }
    public void Restart()
    {
        if (managerExt != null)
        {
            managerExt.OnGameEnd(true);
        }
    }
}
