﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
public class PlayerManager : NetworkBehaviour
{
    [SerializeField] [SyncVar] public int myScore;
    [SerializeField] public bool winStatus;
    public bool getCypherCase = false;
    private bool cooldown = false;
    private Camera minimapCam;
    private void Awake()
    {
        Debug.Log($"SERVER MESSAGE : (ADD {gameObject.name} into Score)");
        ScoreSystem.instance.playerScoreLists.Add(gameObject);
    }

    public void Start()
    {
        if (!isLocalPlayer) return;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        GetCameraFollowingPlayer();
        GetMiniMapCameraFollowingPlayer();
    }

    private void Update()
    {
        getScorefromPickupCase();
    }

    private void getScorefromPickupCase()
    {
        if (getCypherCase)
        {
            if (isServer)
            {
                RpcPickupCase();
            }
            else
            {
                CmdpickupCase();
            }
        }
    }

    private void GetCameraFollowingPlayer()
    {
        Camera.main.GetComponent<CameraFollowing>().setTarget(gameObject.transform);
    }
    private void GetMiniMapCameraFollowingPlayer()
    {
        minimapCam = GameObject.Find("minimapCamera").GetComponent<Camera>();
        if (minimapCam == null) return;
        minimapCam.GetComponent<MiniMapFollowPlayer>().setTarget(gameObject.transform);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "score")
        {
            CmdpickupScore(collision.gameObject);
        }
    }


    [Command(ignoreAuthority = true)]
    public void CmdpickupScore(GameObject scoreobject)
    {
        myScore = myScore + 1;
        RpcPickupScore(scoreobject);
    }

    [ClientRpc]
    public void RpcPickupScore(GameObject scoreobject)
    {
        scoreobject.SetActive(false);
        StartCoroutine(StartReactive(scoreobject));
    }


    [Command(ignoreAuthority = true)]
    public void CmdpickupCase()
    {
        myScore += 5;
        RpcPickupCase();
    }

    [ClientRpc]
    public void RpcPickupCase()
    {
        if (!cooldown)
        {
            cooldown = true;
            StartCoroutine(CypherCaseScore());
        }
    }


    private IEnumerator StartReactive(GameObject scoreobject)
    {
        yield return new WaitForSeconds(5f);
        scoreobject.SetActive(true);
    }

    private IEnumerator CypherCaseScore()
    {
        yield return new WaitForSeconds(3.5f);
        if (cooldown) cooldown = false;
    }
}
