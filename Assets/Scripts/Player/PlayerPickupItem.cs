﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerPickupItem : NetworkBehaviour
{
    public static float slots = 0;
    public static float slot1 = 0;
    public static float slot2 = 0;

    public static float itemSlot1Type = 0;
    public static float itemSlot2Type = 0;
    public float slotNum = 0;


    private GameObject slotTransform1;
    private GameObject slotTransform2;
    public GameObject itemButton;
    public GameObject potion1;
    public GameObject potion2;
    public GameObject potion3;
    public GameObject potion4;

    public GameObject item;
    public Transform itemSpawnPoint;


    private int pointPosition;

    private void Start()
    {
        slotTransform1 = GameObject.Find("Joybutton Item 1");
        slotTransform2 = GameObject.Find("Joybutton Item 2");
        potion1 = GameObject.Find("Potion1");
        potion2 = GameObject.Find("Potion2");
        potion3 = GameObject.Find("Potion3");
        potion4 = GameObject.Find("Potion4");
    }
    public void OnTriggerEnter(Collider other)
    {
        //Pickup Item
        if (other.tag == "Item")
        {
            //if slot not full 
            if (slots != 2)
            {
                if (!hasAuthority)
                {
                    return;
                }
                //pointPosition = other.GetComponent<Item1>().pointPosition;
                if (slot1 == 0)
                {
                    itemSlot1Type = other.GetComponent<Item1>().type;
                    slot1 = 1;
                    AddItem(itemSlot1Type, slot1);
                    PickupItem(other);
                }
                else if (slot2 == 0)
                {
                    itemSlot2Type = other.GetComponent<Item1>().type;
                    slot2 = 2;
                    AddItem(itemSlot2Type, slot2);
                    PickupItem(other);
                }
                slots++;
                //DestroyItem(other.gameObject);
                //StartCoroutine(CooldownSpawnItem(pointPosition));

            }

        }

    }

    private void PickupItem(Collider other)
    {
        if (isServer)
        {
            RpcPickupItem(other.gameObject);
        }
        else
        {
            CmdPickupItem(other.gameObject);
        }
    }

    private IEnumerator StartReactive(GameObject itemobject)
    {
        yield return new WaitForSeconds(5f);
        itemobject.SetActive(true);
    }

    [Command(ignoreAuthority = true)]
    public void CmdPickupItem(GameObject itemobject)
    {
        RpcPickupItem(itemobject);
    }

    [ClientRpc]
    public void RpcPickupItem(GameObject itemobject)
    {
        itemobject.SetActive(false);
        StartCoroutine(StartReactive(itemobject));
    }

    /*
    [Command(ignoreAuthority = true)]
    void CmdSpawnItem()
    {
        SpawnItem.itemCount--;
        var itemClone = (GameObject)Instantiate(item, itemSpawnPoint.transform.position, Quaternion.identity);
        NetworkServer.Spawn(itemClone);
      
    }

    private IEnumerator CooldownSpawnItem(int point)
    {
        yield return new WaitForSeconds(8f);
        CmdSpawned(point);

    }

    [Command(ignoreAuthority = true)]
    private void CmdSpawned(int point)
    {
        switch (point)
        {
            case 0:
                SpawnItem.isPointFull[point] = false;
                SpawnItem.itemCount--;
                break;
            case 1:
                SpawnItem.isPointFull[point] = false;
                SpawnItem.itemCount--;
                break;
            case 2:
                SpawnItem.isPointFull[point] = false;
                SpawnItem.itemCount--;
                break;
            case 3:
                SpawnItem.isPointFull[point] = false;
                SpawnItem.itemCount--;
                break;
            case 4:
                SpawnItem.isPointFull[point] = false;
                SpawnItem.itemCount--;
                break;
            case 5:
                SpawnItem.isPointFull[point] = false;
                SpawnItem.itemCount--;
                break;
            case 6:
                SpawnItem.isPointFull[point] = false;
                SpawnItem.itemCount--;
                break;
            case 7:
                SpawnItem.isPointFull[point] = false;
                SpawnItem.itemCount--;
                break;
            case 8:
                SpawnItem.isPointFull[point] = false;
                SpawnItem.itemCount--;
                break;
            case 9:
                SpawnItem.isPointFull[point] = false;
                SpawnItem.itemCount--;
                break;
            
        }
    }


    private void DestroyItem(GameObject item)
    {
        if (isServer)
        {
            RpcDestroyItem(item);
        }
        else
        {
            CmdDestroyItem(item);
        }
    }
    */


    private void AddItem(float itemType, float slotNum)
    {

        if (isServer)
        {
            RpcAddItem(itemType, slotNum);
        }
        else
        {
            CmdAddItem(itemType, slotNum);
        }



    }


    #region Server
    [ClientRpc]
    private void RpcAddItem(float itemType, float slotNum)
    {
        if (!hasAuthority)
        {
            return;
        }
        if (slotNum == 1)
        {
            switch (itemType)
            {
                case 1:
                    Instantiate(potion1, slotTransform1.transform, false);
                    break;
                case 2:
                    Instantiate(potion2, slotTransform1.transform, false);
                    break;
                case 3:
                    Instantiate(potion3, slotTransform1.transform, false);
                    break;
                case 4:
                    Instantiate(potion4, slotTransform1.transform, false);
                    break;
                default:
                    Debug.Log("Default");
                    break;
            }
        }
        if (slotNum == 2)
        {
            switch (itemType)
            {
                case 1:

                    Instantiate(potion1, slotTransform2.transform, false);
                    break;
                case 2:
                    Instantiate(potion2, slotTransform2.transform, false);
                    break;
                case 3:
                    Instantiate(potion3, slotTransform2.transform, false);
                    break;
                case 4:
                    Instantiate(potion4, slotTransform2.transform, false);
                    break;
                default:
                    Debug.Log("Default");
                    break;

            }
        }

    }

    [ClientRpc]
    private void RpcDestroyItem(GameObject item)
    {
        Destroy(item.gameObject);
    }
    #endregion



    #region Client
    [Command]
    private void CmdAddItem(float itemType, float slotNum)
    {
        RpcAddItem(itemType, slotNum);

    }

    [Command(ignoreAuthority = true)]
    private void CmdDestroyItem(GameObject item)
    {
        RpcDestroyItem(item);
    }
    #endregion



}
