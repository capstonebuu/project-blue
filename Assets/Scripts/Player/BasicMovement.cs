﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class BasicMovement : NetworkBehaviour
{

    public float jumpSeeed;
    float angle = 0;
    public static float relase_angle = 0;
    public bool isInteract { get; set; }
    public bool isHoldObj { get; set; }
    public bool isStun { get; set; }
    public bool isCooldownStun { get; set; }

    [SerializeField] 
    [SyncVar] public float speed;

    [SyncVar]
    private float currentSpeed;




    private Rigidbody rb;
    private Vector3 movement;
    public static string movement_string;
    public static Vector3 movement_vector3;
    public Animator movement_animator;


    private Transform carryPoint;

    private bool isJump = false;

    [Header("Graphics Player")]
    [SerializeField] private Transform graphcisPlayer;

    public override void OnStartAuthority()
    {
        enabled = true;
    }

    void Start()
    {
        getComponents();
    }

    private void FixedUpdate()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        Cmdmove();
    }

    private void Cmdmove()
    {
        // Valudate logic here
        Movement();
    }

    private void Movement()
    {
        if (isCooldownStun)
        {
            StartCoroutine(CooldownStun());
        }
        if (!isInteract)
        {
            MovementDirection();
            RotationAngle();
            //Jumping();
        }
        if (isInteract) { DeactiveMoveAnimation(); }
        AnimationStun();


    }

    private IEnumerator CooldownStun()
    {
        yield return new WaitForSeconds(3f);
        isStun = false;
        isInteract = false;
        isCooldownStun = false;
    }

    private void getComponents()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    private void Jumping()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !isJump)
        {
            rb.AddForce(Vector3.up * jumpSeeed, ForceMode.Impulse);
        }
    }

    private void MovementDirection()
    {
        //movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        //rb.AddForce(movement * speed);
        if (JoystickController.Horizontal_Axis >= .2f) { movement = new Vector3(1, 0, JoystickController.Vertical_Axis); }
        else if (JoystickController.Horizontal_Axis <= -.2f) { movement = new Vector3(-1, 0, JoystickController.Vertical_Axis); }
        else if (JoystickController.Vertical_Axis >= .2f) { movement = new Vector3(JoystickController.Horizontal_Axis, 0, 1); }
        else if (JoystickController.Vertical_Axis <= -.2f) { movement = new Vector3(JoystickController.Horizontal_Axis, 0, -1); }
        else
        {
            movement = new Vector3(0, 0, 0);
        }

        //Debug.Log("Player Speed :" + speed);
        //Debug.Log(speed);
        MoveAnimation();
        movement_vector3 = movement;
        movement_string = movement.ToString();

        //rb.AddForce(movement * speed);
        transform.Translate((movement * speed) * Time.deltaTime);
    }


    private void RotationAngle()
    {

        if (JoystickController.Horizontal_Axis > 0)
        {
            angle = 90;
            angle -= 90 * JoystickController.Vertical_Axis;
            relase_angle = angle;
        }
        else if (JoystickController.Horizontal_Axis < 0)
        {
            angle = -90;
            angle += 90 * JoystickController.Vertical_Axis;
            relase_angle = angle;
        }
        else
        {
            angle = relase_angle;
        }
        //Debug.Log(relase_angle);
        Vector3 v = new Vector3(0, angle, 0);
        graphcisPlayer.localRotation = Quaternion.Euler(v);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Ground")
        {
            isJump = false;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Ground")
        {
            isJump = true;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.gray;
        carryPoint = GameObject.Find("CarryPoint").GetComponent<Transform>();
        if (carryPoint != null)
        {
            Gizmos.DrawSphere(carryPoint.transform.position, 0.2f);
        }
    }

    public void speedDown(float reduceSpeed)
    {
        speed = reduceSpeed;
    }

    #region Animation
    private void MoveAnimation()
    {
        if (movement == new Vector3(0, 0, 0))
        {
            DeactiveMoveAnimation();
        }
        else
        {
            ActiveMoveAnimation();
        }
    }
    private void AnimationStun()
    {
        if (isStun)
        {
            ActiveStunAnimation();
        }
        else if (!isStun)
        {
            DeactiveStunAnimation();
        }
    }


    private void ActiveMoveAnimation()
    {
        if (isServer)
        {
            RpcActiveMove();
        }
        else
        {
            CmdActiveMove();
        }
    }

    private void DeactiveMoveAnimation()
    {
        if (isServer)
        {
            RpcDeactiveMove();
        }
        else
        {
            CmdDeactiveMove();
        }
    }



    private void DeactiveStunAnimation()
    {
        if (isServer)
        {
            RpcDeactiveStun();
        }
        else
        {
            CmdDeactiveStun();
        }
    }

    private void ActiveStunAnimation()
    {
        if (isServer)
        {
            RpcActiveStun();
        }
        else
        {
            CmdActiveStun();
        }
    }
    #endregion

    #region Server
    [ClientRpc]
    private void RpcActiveMove()
    {
        movement_animator.SetBool("isMoving", true);
    }

    [ClientRpc]
    private void RpcDeactiveMove()
    {
        movement_animator.SetBool("isMoving", false);
    }

    [ClientRpc]
    private void RpcActiveStun()
    {
        movement_animator.SetBool("isStuning", true);
    }

    [ClientRpc]
    private void RpcDeactiveStun()
    {
        movement_animator.SetBool("isStuning", false);
    }



    #endregion

    #region Client
    [Command]
    void CmdActiveMove()
    {
        RpcActiveMove();
    }

    [Command]
    void CmdDeactiveMove()
    {
        RpcDeactiveMove();
    }

    [Command]
    void CmdActiveStun()
    {
        RpcActiveStun();
    }

    [Command]
    void CmdDeactiveStun()
    {
        RpcDeactiveStun();
    }
    #endregion


}
