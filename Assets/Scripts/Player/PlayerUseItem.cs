﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class PlayerUseItem : NetworkBehaviour
{
    private GameObject itemUi;
    public GameObject item;
    public GameObject item2;
    public GameObject item3;

    private float slotNum;
    private GameObject playerTransform;
    public Transform player;
    public Joybutton_Interact itemButton1;
    public Joybutton_Interact itemButton2;

    private Vector3 spawnPosition;

    [SerializeField] ParticleSystem SpeedUpParticles;
    // Update is called once per frame
    public override void OnStartClient()
    {
        base.OnStartClient();
        itemButton1 = GameObject.Find("Joybutton Item 1").GetComponent<Joybutton_Interact>();
        itemButton2 = GameObject.Find("Joybutton Item 2").GetComponent<Joybutton_Interact>();
        SpeedUpParticles.Stop();
    }
    void Update()
    {


        if (!isLocalPlayer) { return; }
        //use item slot1
        if (itemButton1 != null)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) || itemButton1.Pressed)
            {
                if (PlayerPickupItem.slot1 != 0)
                {
                    itemUi = GameObject.Find("Joybutton Item 1");
                    Destroy(itemUi.transform.GetChild(0).gameObject);

                    switch (PlayerPickupItem.itemSlot1Type)
                    {
                        case 1:
                            StartCoroutine(CooldownSpawnItem(1));
                            break;
                        case 2:
                            StartCoroutine(CooldownSpawnItem(2));
                            break;
                        case 3:
                            StartCoroutine(UpSpeed());
                            break;
                        case 4:
                            StartCoroutine(CooldownSpawnItem(3));
                            break;
                        default:
                            break;
                    }
                    PlayerPickupItem.slots--;
                    PlayerPickupItem.slot1 = 0;
                }
            }
        }

        //use item slot2
        if (itemButton2 != null)
        {
            if (Input.GetKeyDown(KeyCode.Alpha2) || itemButton2.Pressed)
            {
                if (PlayerPickupItem.slot2 != 0)
                {
                    itemUi = GameObject.Find("Joybutton Item 2");
                    Destroy(itemUi.transform.GetChild(0).gameObject);
                    switch (PlayerPickupItem.itemSlot2Type)
                    {
                        case 1:
                            StartCoroutine(CooldownSpawnItem(1));
                            break;
                        case 2:
                            StartCoroutine(CooldownSpawnItem(2));
                            break;
                        case 3:
                            StartCoroutine(UpSpeed());
                            break;
                        case 4:
                            StartCoroutine(CooldownSpawnItem(3));
                            break;
                        default:
                            break;
                    }
                    PlayerPickupItem.slots--;
                    PlayerPickupItem.slot2 = 0;
                }

            }
        }
    }

    private IEnumerator CooldownSpawnItem(float type)
    {
        spawnPosition = player.transform.position;
        yield return new WaitForSeconds(1f);
        CmdSpawnItem(type, spawnPosition);
    }
    private IEnumerator UpSpeed()
    {
        UpdateSpeed(7);
        yield return new WaitForSeconds(5f);
        UpdateSpeed(5);

    }
    private void UpdateSpeed(float speedValue)
    {
        if (isServer)
        {
            RpcChangeSpeed(speedValue);
        }
        else
        {
            CmdChangeSpeed(speedValue);
        }
    }

    #region Server
    /*[ClientRpc]
    private void RpcSpawnItem()
    {
        var itemClone = (GameObject)Instantiate(item, player.transform.position, Quaternion.identity);
        //GameObject itemClone = Instantiate(item);
        NetworkServer.Spawn(itemClone);
        //NetworkServer.Spawn(Instantiate(item, player.transform.position, Quaternion.identity));
    }
    */
    [ClientRpc]
    public void RpcChangeSpeed(float value)
    {
        this.GetComponent<BasicMovement>().speed = value;
        if (value <= 5)
        {
            SpeedUpParticles.Stop();
        }
        else if (value >= 7)
        {
            SpeedUpParticles.Play();
        }

    }
    #endregion


    [Command(ignoreAuthority = true)]
    public void CmdChangeSpeed(float value)
    {
        RpcChangeSpeed(value);
    }
    [Command(ignoreAuthority = true)]
    void CmdSpawnItem(float type, Vector3 pos)
    {
        //SpawnItem.itemCount--;

        if (type == 1)
        {
            var itemClone = (GameObject)Instantiate(item, pos + transform.forward, Quaternion.identity);
            NetworkServer.Spawn(itemClone);
        }
        if (type == 2)
        {
            var itemClone = (GameObject)Instantiate(item2, pos + transform.forward, Quaternion.identity);
            NetworkServer.Spawn(itemClone);
        }
        if (type == 3)
        {
            var itemClone = (GameObject)Instantiate(item3, pos + transform.forward, Quaternion.identity);
            NetworkServer.Spawn(itemClone);
        }


    }
}
