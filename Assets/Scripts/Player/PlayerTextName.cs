﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerTextName : MonoBehaviour
{
    [SerializeField]
    public TextMesh tm;

    private string playerID;
    private Transform t;
    // Start is called before the first frame update
    void Start()
    {
        t = transform;
        tm = GetComponent<TextMesh>();
    }

    // Update is called once per frame
    void Update()
    {
        t.eulerAngles = new Vector3(t.eulerAngles.x, 5, t.eulerAngles.z);
        tm.text = PlayerNameInput.DisplayName;
    }
}
