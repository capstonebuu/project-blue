﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : NetworkBehaviour
{
    [SerializeField]
    private GameObject Hitbox;
    public GameObject MainObjective;
    private bool start = false;

    public Animator attack_animator;


    public static bool isAttack = false;
    // Start is called before the first frame update
    void Start()
    {
        Hitbox = transform.GetChild(3).gameObject;
    }


    #region Server

    [ClientRpc]
    private void RpcActiveAttack()
    {
        if (Hitbox == null) { return; }
        Debug.Log("attack");
        attack_animator.SetBool("isAttack", true);
        isAttack = true;
        Hitbox.SetActive(true);
    }

    [ClientRpc]
    private void RpcDeActiveAttack()
    {
        if (Hitbox == null) { return; }
        attack_animator.SetBool("isAttack", false);
        Hitbox.SetActive(false);
        isAttack = false;
    }

    #endregion

    #region Client

    // Update is called once per frame
    private void Update()
    {
        if (!hasAuthority) { return; }

        if (Input.GetKeyDown(KeyCode.Q) && isAttack == false)
        {
            if (isServer)
            {
                RpcActiveAttack();
            }
            else
            {
                CmdActiveAttack();
            }
            StartCoroutine(Delay(0.7f));
        }
    }

    private IEnumerator Delay(float cooldown)
    {
        Debug.Log("delay");
        yield return new WaitForSeconds(cooldown);
        if (isServer)
        {
            RpcDeActiveAttack();
        }
        else
        {
            CmdDeActiveAttack();
        }
    }


    private IEnumerator OnTriggerEnter(Collider other)
    {
        Debug.Log("Enter");
        //if (other.TryGetComponent<BasicMovement>(out BasicMovement basic))
        if (other.tag == "Player" && isAttack == true)
        {
            Debug.Log("Hit");
            other.GetComponent<BasicMovement>().isInteract = true;
            other.GetComponent<BasicMovement>().isStun = true;
            yield return new WaitForSeconds(3f);
            other.GetComponent<BasicMovement>().isInteract = false;
            other.GetComponent<BasicMovement>().isStun = false;
        }
    }

    [Command]
    void CmdActiveAttack()
    {
        RpcActiveAttack();
    }

    [Command]
    void CmdDeActiveAttack()
    {
        RpcDeActiveAttack();
    }
    #endregion


}
