﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interactable_valveActive : MonoBehaviour
{
    Collider ValveCollider;
    public static bool valveActive = false;
    [SerializeField] Joybutton_Interact interactButton;
    [SerializeField] public interactable_waterPipeActive waterpipe;
    // Start is called before the first frame update
    void Start()
    {
        ValveCollider = GetComponent<BoxCollider>();
        //Debug.Log(ValveCollider);
    }

    private void Update()
    {
        if (interactButton == null)
        {
            interactButton = GameObject.FindWithTag("InteractButton").GetComponent<Joybutton_Interact>();
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (waterpipe.isWaterPipeActive == false && interactButton != null)
        {
            interactButton.interactable = true;
            if (other.tag == "Player" && (Input.GetKeyDown(KeyCode.E) || interactButton.Pressed))
            {
                waterpipe.isWaterPipeActive = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && interactButton != null)
        {
            interactButton.interactable = false;
        }
    }
}
