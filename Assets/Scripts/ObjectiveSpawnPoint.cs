﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveSpawnPoint : MonoBehaviour
{

    public GameObject objectiveSpawnPoint;
    public GameObject objective;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Init : Main Obj Spawn at " + objectiveSpawnPoint.transform.position);
        Instantiate(objective, objectiveSpawnPoint.transform.position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
