﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class interactable_openDoor : NetworkBehaviour
{
    public Transform Door;
    public Transform Door2;
    public Transform closeLocation2;
    public Transform closeLocation;
    public Transform openLocation2;
    public Transform openLocation;
    public bool isOpen = false;
    public bool isMoving = false;
    public bool DoorStatus = false;
    float ypos;
    float xpos;
    int type;
    float tempXpos;
    float tempXpos2;
    private Transform closeLocationTemp, closeLocationTemp2;
    private MeshRenderer renderer1, renderer2;

    [SerializeField] public Joybutton_Interact interactButton;
    public enum DoorType
    {
        SingleDoorV,
        SingleDoorH,
        DoubleDoorH
    }
    public DoorType wtype;
    // Start is called before the first frame update
    void Start()
    {

        renderer1 = Door.GetComponent<MeshRenderer>();
        //renderer2 = Door2.GetComponent<MeshRenderer>();

        //closeLocationTemp.position = Door.localPosition;
        //tempXpos2 = Door2.localPosition.x;
        if (wtype.Equals(DoorType.DoubleDoorH))
        {
            renderer2 = Door2.GetComponent<MeshRenderer>();
            tempXpos2 = Door2.localPosition.x;
            closeLocationTemp2.position = Door2.localPosition;
        }
    }
    // Update is called once per frame
    void Update()
    {
        //DoorV.transform.position += new Vector3(0, ypos * Time.deltaTime, 0);
        /*
        if (wtype.Equals(DoorType.SingleDoorV))
        {
            type = 1;
            type1Active();
        }
        if (wtype.Equals(DoorType.DoubleDoorH))
        {
            type = 2;
            type2Active();
        }
        if (wtype.Equals(DoorType.SingleDoorH))
        {
            type = 3;
            
        }
        */
        if (DoorStatus)
        {
            if (isServer)
            {
                RpcMovingDoor(false, true, 2f);
            }
            else
            {
                CmdMovingDoor(false, true, 2f);
            }
        }
        else
        {
            if (isServer)
            {
                RpcMovingDoor(true, true, -2f);
            }
            else
            {
                CmdMovingDoor(true, true, -2f);
            }
        }
    }



    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if ((Input.GetKeyDown(KeyCode.E) || interactButton.Pressed) && isOpen)
            {
                if (isServer)
                if (isMoving)
                {
                    return;
                }
                if (type == 1)
                {
                    ypos = 2;
                    Debug.Log("Close Door in vertical");
                }
                if (type == 2)
                {
                    RpcMovingDoor(false, true, 2f);
                }
                else
                {
                    CmdMovingDoor(false, true, 2f);
                    xpos = 2;
                    //type3Active();
                    Debug.Log("Close Door in horizontal");
                }
                /*
                xpos = 2;
                Debug.Log("Close Door in horizontal");
                isOpen = false;
                isMoving = true;
                */
                //yield return new WaitForSeconds(2f);
                
                
            }
            /*
            else if ((Input.GetKeyDown(KeyCode.E) || interactButton.Pressed) && !isOpen)
            {
                if (transform.parent.GetComponent<NetworkIdentity>().isServer)
                if (isMoving)
                {
                    return;
                }
                if (type == 1)
                {
                    ypos = -2f;
                    Debug.Log("Open Door in vertical");
                }
                if (type == 2)
                {
                    RpcMovingDoor();
                }
                else if (transform.parent.GetComponent<NetworkIdentity>().isLocalPlayer)
                {
                    CmdMovingDoor();
                    xpos = -2f;
                    //type3Active();
                    Debug.Log("Open Door in horizontal");
                }
                Debug.Log("Open Door in horizontal");
                ypos = -2f;
                //Debug.Log("Open Door in vertical");

                isOpen = true;
                isMoving = true;

                //yield return new WaitForSeconds(2f);
               
                
            }
            */
        }
    }
    #region unused
    /*

    #region Notuse

    private void type1Active()
    {
        if (Door.position.y > closeLocation.position.y)
        {
            Door.position = closeLocation.position;
            isMoving = false;
        }
        else if (Door.position.y < openLocation.position.y)
        {
            Door.position = openLocation.position;
            isMoving = false;
        }
        else if (isMoving)
        {
            Door.transform.position += new Vector3(0, ypos * Time.deltaTime, 0);
        }
    }
    private void type2Active()
    {
        if (Door.localPosition.x > tempXpos + renderer1.bounds.size.x)
        {
            Door.transform.position = new Vector3(tempXpos + renderer1.bounds.size.x, Door.position.y, Door.position.z);
            Door.transform.position = new Vector3(tempXpos2 - renderer2.bounds.size.x, Door.position.y, Door.position.z);
            Debug.Log("Temp X pos : " + tempXpos);
            isMoving = false;
        }
        else if (Door.localPosition.x < tempXpos)
        {
            Door.transform.position = new Vector3(tempXpos, Door.position.y, Door.position.z);
            Door.transform.position = new Vector3(tempXpos2, Door.position.y, Door.position.z);
            Debug.Log("Temp X pos : " + tempXpos);
            isMoving = false;
        }
        else if (isMoving)
        {
            Door.transform.position += new Vector3(xpos * Time.deltaTime, 0, 0);
            Door2.transform.position -= new Vector3(xpos * Time.deltaTime, 0, 0);
        }
    }
   #endregion
    */

    #endregion


    private IEnumerator openDoor()
    {
        yield return new WaitForSeconds(2f);
        isOpen = true;
    }

    private IEnumerator closeDoor()
    {
        yield return new WaitForSeconds(2f);
        isOpen = false;
    }

    #region Client 
    [Command(ignoreAuthority = true)]
    public void CmdOpenDoor()
    {
        RpcOpenDoor();
        
    }

    [Command(ignoreAuthority = true)]
    public void CmdCloseDoor()
    {
        RpcCloseDoor();
    }

    [Command(ignoreAuthority = true)]
    public void CmdMoving()
    {
        RpcMoving();
    }
    #endregion

    #region Server 
    [ClientRpc]
    public void RpcOpenDoor()
    {
        Door.position = openLocation.position;
    }
    [ClientRpc]
    public void RpcCloseDoor()
    {
        Door.position = closeLocation.position;
    }
    [ClientRpc]
    public void RpcMoving()
    {
        Door.transform.position += new Vector3(xpos * Time.deltaTime, 0, 0);
    }
    #endregion



    private void type3Active()
    {
        if (Door.position.x > closeLocation.position.x)
        {
        
            CloseDoor();
            isMoving = false;
            DoorStatus = false;
        }
        else if (Door.position.x < openLocation.position.x)
        {
            OpenDoor();
            isMoving = false;
        }
        else if (isMoving)
        {
            CmdMoving();
        }
    }

    [Command(ignoreAuthority = true)]
    public void CmdMovingDoor(bool doorStatus, bool doorMoving, float speed)
    {
        RpcMovingDoor(doorStatus, doorMoving, speed);
    }
    [ClientRpc]
    public void RpcMovingDoor(bool doorStatus, bool doorMoving, float speed)
    {
        isOpen = doorStatus;
        isMoving = doorMoving;
        xpos -= speed;
        type3Active();
    }
    private void OpenDoor()
    {
        if (isServer)
        {
            RpcOpenDoor();
        }
        else
        {
            CmdOpenDoor();
        }
    }
    private void CloseDoor()
    { 
        if (isServer)
        {
            RpcCloseDoor();
        }
        else
        {
            CmdCloseDoor();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && other.gameObject.GetComponent<PlayerManager>().isLocalPlayer)
        {
            interactButton.interactable = false;
        }
    }
}
