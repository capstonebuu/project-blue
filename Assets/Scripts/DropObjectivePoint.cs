﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class DropObjectivePoint : NetworkBehaviour
{


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && MainObjectivePickupDetector.isCarry)
        {
            CheckWin checkWin = GameObject.Find("GameController").GetComponent<CheckWin>();
            checkWin.condition = true;
        }
    }
}
