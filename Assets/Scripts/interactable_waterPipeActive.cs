﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interactable_waterPipeActive : NetworkBehaviour
{
    Collider ObjectCollider;
    public bool isWaterPipeActive { get; set; }
    //public bool coroutine;
    private float duration = 0;
    private bool isCooldown = false;

    [SerializeField] ParticleSystem waterPipeParticles;

    [SerializeField] AudioClip waterPipeAudio;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material.color = Color.green;
        ObjectCollider = GetComponent<BoxCollider>();
        ObjectCollider.enabled = false;
        waterPipeParticles.Stop();

    }


    // Update is called once per frame
    void LateUpdate()
    {
        BlockDuration();
        FixBugCooldown();
    }

    private void FixBugCooldown()
    {
        if (isCooldown)
        {
            duration += Time.deltaTime;
            if (duration >= 0.5f)
            {
                CmdDeactiveWaterPipe();
                isCooldown = false;
                duration = 0f;
            }
        }
    }

    private void BlockDuration()
    {
        if (isWaterPipeActive)
        {
            CmdActiveWaterPipe();
            duration += Time.deltaTime;
            if (duration >= 5f)
            {
                CmdDeactiveWaterPipe();
                isCooldown = true;
                duration = 0f;
            }

        }
    }


    #region Server 
    [ClientRpc]
    public void RpcActiveWaterPipe()
    {
        //if (!audioSource.isPlaying) { audioSource.PlayOneShot(waterPipeAudio); }
        waterPipeParticles.Play();
        GetComponent<Renderer>().material.color = Color.red;
        ObjectCollider.enabled = true;
    }

    [ClientRpc]
    public void RpcDeactiveWaterPipe()
    {
        waterPipeParticles.Stop();
        //audioSource.PlayOneShot(waterPipeAudio);
        GetComponent<Renderer>().material.color = Color.green;
        ObjectCollider.enabled = false;
        isWaterPipeActive = false;


    }
    #endregion

    #region Client 
    [Command(ignoreAuthority = true)]
    public void CmdActiveWaterPipe()
    {
        RpcActiveWaterPipe();
    }

    [Command(ignoreAuthority = true)]
    public void CmdDeactiveWaterPipe()
    {
        RpcDeactiveWaterPipe();
    }
    #endregion

}
