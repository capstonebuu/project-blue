﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interactable_jump : MonoBehaviour
{

    public GameObject InteractableWallS_1;
    public Transform colliderTop;
    public Transform colliderBot;
    [SerializeField] public Joybutton_Interact interactButton;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            interactButton.interactable = true;
            Debug.Log("Stay");
            if ((Input.GetKeyDown(KeyCode.E) || interactButton.Pressed))
            {
                BasicMovement.relase_angle = transform.eulerAngles.y;
                Debug.Log("wait for jump");
                yield return new WaitForSeconds(2);
                Debug.Log("Jump");
                other.transform.position += other.transform.forward * 2.6f;

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            interactButton.interactable = false;
        }
    }
}
