﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class SceneMenuGUI : MonoBehaviour
{

    public static SceneMenuGUI instance;
    [SerializeField] GameObject lobbyCanvas;

    [Header("Menu Panel")]
    [SerializeField] private GameObject panelMatch;

    [SerializeField] private Transform PlayerLobbyUI;
    [SerializeField] private GameObject PlayerLobbyUIPrefab;

    private void Start()
    {
        instance = this;
    }

    public void updateCanvas()
    {
        lobbyCanvas.SetActive(false);
    }
    /*
    public void SpawnPlayerLobbyUIPrefab(NetworkRoomPlayerExt player)
    {
        //GameObject newPlayerLobbyUI = Instantiate(PlayerLobbyUIPrefab, PlayerLobbyUI);
        //newPlayerLobbyUI.GetComponent<PlayerLobbyUI>().setPlayer(player);
    }
    */
}
