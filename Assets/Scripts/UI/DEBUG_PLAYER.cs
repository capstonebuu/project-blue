﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
public class DEBUG_PLAYER : NetworkBehaviour
{
    [SerializeField] private Text DEBUG_PLAYER_SPEED = null;
    [SerializeField] private Text DEBUG_PLAYER_POSIOTN = null;

    private void Update()
    {
        if (DEBUG_PLAYER_SPEED == null && DEBUG_PLAYER_POSIOTN == null)
        {
            DEBUG_PLAYER_SPEED = GameObject.Find("Player Speed").GetComponent<Text>();
            DEBUG_PLAYER_POSIOTN = GameObject.Find("Player Positions").GetComponent<Text>();
        }
        else
        {
            if (hasAuthority)
            {
                BasicMovement basicMovement = GetComponent<BasicMovement>();
                DEBUG_PLAYER_SPEED.text = "Player Speed : " + basicMovement.speed.ToString();
                DEBUG_PLAYER_POSIOTN.text = "Player Positions : " + BasicMovement.movement_string;
            }
        }
    }
    private void OnDestroy()
    {
        DEBUG_PLAYER_SPEED = null;
        DEBUG_PLAYER_POSIOTN = null;
    }
}
