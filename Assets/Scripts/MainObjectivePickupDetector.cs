﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainObjectivePickupDetector : MonoBehaviour
{

    public Transform MainObjective;
    public static Transform MainObjectiveObj;
    public Transform carryPoint;
    public static bool isPassingDropPoint = false;
    public static bool canbeCarry = false;
    public static bool isCarry = false;
    // Start is called before the first frame update
    void Start()
    {
        canbeCarry = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (MainObjective == null)
        {
            MainObjective = GameObject.FindWithTag("MainObjective").GetComponent<Transform>();
            Debug.Log("Null : Transform position MainObjective to Tag:MainObjective at " + MainObjective.transform.position);
        }

        if (MainObjectiveObj == null)
        {
            MainObjectiveObj = MainObjective.transform.GetChild(0).GetComponent<Transform>();
            //MainObjectiveObj = GameObject.FindWithTag("MainObjective").GetComponentInChildren<GameObject>();
            //Debug.Log("Null : Transform position MainObjectiveObj to Tag:Objective at " + MainObjectiveObj.transform.position);
        }

        if (carryPoint == null)
        {
            //carryPoint = GameObject.FindWithTag("Carry").GetComponent<Transform>();
            //Debug.Log("Null : Transform position carryPoint to Tag:Carry at " + carryPoint.transform.position);
        }

        if (isPassingDropPoint)
        {
            MainObjectiveObj.gameObject.SetActive(false);
            isPassingDropPoint = false;
        }
        if (isCarry)
        {
            MainObjective.transform.position = carryPoint.transform.position;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isCarry = true;
            carryPoint = other.gameObject.GetComponent<Transform>().Find("CarryPoint");
            other.GetComponent<PlayerManager>().getCypherCase = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerManager>().getCypherCase = false;
        }
    }

    private void OnDestroy()
    {
        resetVariables();
    }

    private void resetVariables()
    {
        isPassingDropPoint = false;
        canbeCarry = false;
        isCarry = false;
    }
}
