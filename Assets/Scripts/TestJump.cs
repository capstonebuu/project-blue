﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestJump : MonoBehaviour
{
    public GameObject obj;
    public Transform player;
    private bool isPlayerEnter = false;
    private Transform landPoint;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(obj.transform.GetChild(0).name+" : "+ obj.transform.GetChild(1).name +" : " + obj.transform.GetChild(2).name);
      
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlayerEnter && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Jump");
            player.transform.position = new Vector3(landPoint.transform.position.x,0.65f,landPoint.transform.position.z);
            Debug.Log(player.transform.position);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Jump Trigger")
        {
            Debug.Log("Enter Trigger");
            isPlayerEnter = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Leave Trigger");
        isPlayerEnter = false;
    }
}
