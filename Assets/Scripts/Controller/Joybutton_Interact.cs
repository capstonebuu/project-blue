﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Mirror;
public class Joybutton_Interact : NetworkBehaviour, IPointerUpHandler, IPointerDownHandler
{
    [SerializeField] public bool Pressed { get; private set; }
    [SerializeField] bool test;
    private Color oldColor;
    [SerializeField] Color pressedColor;
    public Color interactableColor;
    public bool interactable = false;
    // Start is called before the first frame update
    void Start()
    {
        oldColor = gameObject.GetComponent<Image>().color;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Debug.Log("Do This");
            }
        }
        if (isServer)
        {
            RpcUpdateInteractButton();
        }
        else
        {
            CmdUpdateInteractButton();
        }
    }

    [Command(ignoreAuthority = true)]
    public void CmdUpdateInteractButton()
    {
        if (Pressed)
        {
            GetComponent<Image>().color = pressedColor;
        }
        if (interactable)
        {
            GetComponent<Image>().color = interactableColor;
        }
        else
        {
            GetComponent<Image>().color = oldColor;
        }
    }

    [ClientRpc]
    public void RpcUpdateInteractButton()
    {
        CmdUpdateInteractButton();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Pressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Pressed = false;
    }

    private IEnumerator OnCooldownPressed()
    {
        yield return new WaitForSeconds(2f);
        Pressed = false;
    }
}
