﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickController : MonoBehaviour
{
    private Joystick joystick;
    public static float Horizontal_Axis = 0f;
    public static float Vertical_Axis = 0f;
    void Start()
    {
        joystick = FindObjectOfType<Joystick>();
    }

    private void Update()
    {
        Horizontal_Axis = joystick.Horizontal;
        Vertical_Axis = joystick.Vertical;
    }
}
