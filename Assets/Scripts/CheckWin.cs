﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
public class CheckWin : NetworkBehaviour
{
    public GameObject winScreen;
    public GameObject WinScreenText;
    public GameObject WinScreenButton;
    public Animator anim;

    [SyncVar(hook = nameof(onChangewinstate))]
    bool isWinningCondition = false;

    [SyncVar]
    public bool condition = false;

    private bool checkisReallyWin = false;
    private bool flag = false;
    // Start is called before the first frame update
    void Start()
    {
        WinScreenText.SetActive(false);
        WinScreenButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isWinningCondition == true)
        {
            winScreen.SetActive(true);
            anim.SetBool("isWin", true);
            checkisReallyWin = true;
            Debug.Log("Win");
        }
        else
        {
            winScreen.SetActive(false);
        }

        if (checkisReallyWin && !flag)
        {
            StartCoroutine(displayComponets());
            flag = true;
        }
        isWinningCondition = condition;
    }
    public void onChangewinstate(bool oldValue, bool newValue)
    {
        isWinningCondition = newValue;
    }

    IEnumerator displayComponets()
    {
        Debug.Log("Start Count");
        yield return new WaitForSeconds(1f);
        WinScreenText.SetActive(true);
        WinScreenButton.SetActive(true);

    }

    private void OnDestroy()
    {
        isWinningCondition = false;
        condition = false;
        checkisReallyWin = false;
        flag = false;
    }
}
