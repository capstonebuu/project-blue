﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interactable_teleport : NetworkBehaviour
{
    [SerializeField]
    private Transform teleportTarget;
    bool isPlayerEnter = false;

    [SerializeField] ParticleSystem teleportParticles;
    [SerializeField] Joybutton_Interact interactButton;

    private void Start()
    {
        teleportTarget = transform.GetChild(0).GetComponent<Transform>();
        teleportParticles.Stop();
    }

    IEnumerator OnTriggerStay(Collider other)
    {
        interactButton.interactable = true;
        if (other.tag == "Player" && Input.GetKeyDown(KeyCode.E) || interactButton.Pressed)
        {
            other.GetComponent<BasicMovement>().isInteract = true;
            ActiveEffectTeleport();
            //teleportParticles.Play();
            Debug.Log("Enter Trigger");
            //Print the time of when the function is first called.
            Debug.Log("Started Coroutine at timestamp : " + Time.time);

            yield return new WaitForSeconds(1.5f);

            isPlayerEnter = true;
            DeactiveEffectTeleport();
            //teleportParticles.Stop();
            other.transform.position = teleportTarget.transform.position;
            other.GetComponent<BasicMovement>().isInteract = false;
            //After we have waited 5 seconds print the time again.
            Debug.Log("Finished Coroutine at timestamp : " + Time.time);
        }

    }


    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Leave Trigger");
        isPlayerEnter = false;
        if (other.tag == "Player")
        {
            interactButton.interactable = false;
        }
    }




    private void ActiveEffectTeleport()
    {
        if (isServer)
        {
            RpcActiveEffectTeleport();
        }
        else
        {
            CmdActiveEffectTeleport();
        }
    }

    private void DeactiveEffectTeleport()
    {
        if (isServer)
        {
            RpcDeactiveEffectTeleport();
        }
        else
        {
            CmdDeactiveEffectTeleport();
        }
    }


    #region Server 
    [ClientRpc]
    public void RpcActiveEffectTeleport()
    {
        teleportParticles.Play();
    }

    [ClientRpc]
    public void RpcDeactiveEffectTeleport()
    {
        teleportParticles.Stop();
    }
    #endregion

    #region Client
    [Command(ignoreAuthority = true)]
    void CmdActiveEffectTeleport()
    {
        RpcActiveEffectTeleport();
    }

    [Command(ignoreAuthority = true)]
    void CmdDeactiveEffectTeleport()
    {
        RpcDeactiveEffectTeleport();
    }

    #endregion



}
