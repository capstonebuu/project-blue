﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detecting : MonoBehaviour
{
    public GameObject InteractableWallS;
    public bool onStay;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (onStay == true)
        {
            InteractableWallS.SetActive(false);
        }
        else
        {
            InteractableWallS.SetActive(true);
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player"){
            //InteractableWallS.SetActive(false);
            onStay = true;
            Debug.Log(onStay);
        }
    }
    

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player") {
            //InteractableWallS.SetActive(true);
            onStay = false;
            Debug.Log(onStay);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player"){
            onStay = true;
            Debug.Log("Pass Stay");
        }
    }
}
