﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class interactable_slowPad : NetworkBehaviour
{

    private bool isPlayerEnter = false;
    private bool isColorChanged = false;
    public static bool isOilActive = false;
    private float duration = 0;
    private GameObject player = null;
    private Color objectColor;

    [SerializeField]
    private GameObject effectSlow;

    [SerializeField] ParticleSystem slowParticles;

    private void Start()
    {

        //effectSlow = GameObject.Find("OilFloor/EffectSlow");
        slowParticles.Stop();
        objectColor = GetComponent<Renderer>().material.color;

    }

    private void Update()
    {
        slowDuration();
    }

    private void slowDuration()
    {
        if (isOilActive)
        {
            CmdActiveEffectSlow();
            //ActiveEffectSlow();
            StartCoroutine(DeactiveSlow());
            /*duration += Time.deltaTime;
            if (duration >= 5f)
            {
                DeactiveEffectSlow();
                isOilActive = false;
                isColorChanged = false;
                duration = 0f;
            }*/
        }
    }

    private IEnumerator DeactiveSlow()
    {
        yield return new WaitForSeconds(5f);
        DeactiveEffectSlow();
        isOilActive = false;
        isColorChanged = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            isPlayerEnter = true;
            if (isOilActive && isPlayerEnter)
            {
                if (isServer)
                {
                    RpcChangeSpeed(other.gameObject, 3f, Color.yellow);
                }
                else
                {
                    CmdChangeSpeed(other.gameObject, 3f, Color.yellow);
                }
            }
        }

    }


    private void OnTriggerExit(Collider other)
    {
        isPlayerEnter = false;
        //Increase Speed after Exit
        if (isOilActive)
        {
            if (isServer)
            {
                RpcChangeSpeed(other.gameObject, 5f, objectColor);
            }
            else
            {
                CmdChangeSpeed(other.gameObject, 5f, objectColor);
            }
        }

    }


    private void UpdateSpeed(GameObject target, float speedValue, Color colorValue)
    {
        if (isServer)
        {
            RpcChangeSpeed(target, speedValue, colorValue);
        }
        else
        {
            CmdChangeSpeed(target, speedValue, colorValue);
        }
    }




    #region server
    [ClientRpc]
    public void RpcChangeSpeed(GameObject target, float value, Color colorValue)
    {
        if (target != null)
        {

            target.GetComponent<BasicMovement>().speed = value;
            GetComponent<Renderer>().material.color = colorValue;
        }
    }

    [ClientRpc]
    public void RpcActiveEffectSlow()
    {

        effectSlow.SetActive(true);
        slowParticles.Play();
    }

    [ClientRpc]
    public void RpcDeactiveEffectSlow()
    {
        effectSlow.SetActive(false);
        slowParticles.Stop();
    }
    #endregion


    #region client
    [Command(ignoreAuthority = true)]
    public void CmdChangeSpeed(GameObject target, float value, Color colorValue)
    {
        if (target != null)
        {
            RpcChangeSpeed(target, value, colorValue);
        }
    }

    [Command(ignoreAuthority = true)]
    void CmdActiveEffectSlow()
    {
        RpcActiveEffectSlow();
    }

    [Command(ignoreAuthority = true)]
    void CmdDeactiveEffectSlow()
    {
        RpcDeactiveEffectSlow();
    }
    #endregion

    #region Animation
    public void DeactiveEffectSlow()
    {
        if (isServer)
        {
            RpcDeactiveEffectSlow();
        }
        else
        {
            CmdDeactiveEffectSlow();
        }
    }

    public void ActiveEffectSlow()
    {
        if (isServer)
        {
            RpcActiveEffectSlow();
        }
        else
        {
            CmdActiveEffectSlow();
        }
    }

    #endregion

}
