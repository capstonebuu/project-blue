﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class InteractableHorizontal : NetworkBehaviour
{
    [SerializeField] public bool openDoor = false;
    [SerializeField] private Transform doors;
    [SerializeField] public Joybutton_Interact interactButton;
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && !openDoor)
        {
            if (interactButton.Pressed)
            {
                if (isServer)
                {
                    RpcDoorStatus();
                }
                else
                {
                    CmdDoorStatus();
                }
            }

        }
    }

    [ClientRpc]
    public void RpcDoorStatus()
    {
        doors.position = new Vector3(1.5f, 0, 0);
    }

    [Command(ignoreAuthority = true)]
    public void CmdDoorStatus()
    {
        RpcDoorStatus();
    }
}
