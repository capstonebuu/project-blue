﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class CameraEvents : MonoBehaviour
{
    [SerializeField] private NetworkRoomManager networkManager;
    [SerializeField] private MainMenu c;
    private void Start()
    {
        Application.targetFrameRate = 60;
    }
    public void HostLobby()
    {
        networkManager.StartHost();
    }

    public void StartGame()
    {
        Debug.Log($"Address : {networkManager.networkAddress}");
        networkManager.StartClient();
    }

    public void EnableMenu()
    {
        c.BackToMenu();
        kcp2k.KcpConnection.Kcpdisconnected = false;
    }

    private void Update()
    {
        /*
         * To Avoid this Warninig message
         * Multiple NetworkManagers detected in the scene. Only one NetworkManager can exist at a time. The duplicate NetworkManager will be destroyed.
         */
        if (networkManager == null)
        {
            networkManager = GameObject.FindWithTag("NetworkManager").GetComponent<NetworkRoomManager>();
        }
        if (c == null)
        {
            c = GameObject.Find("Canvas").GetComponent<MainMenu>();
        }
    }
}
