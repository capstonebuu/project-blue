﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapFollowPlayer : MonoBehaviour
{
    [Header("Camera")]
    public Transform player;
    [SerializeField] Vector3 offset;
    private void Start()
    {

    }

    void LateUpdate()
    {
        if (player != null)
        {
            transform.position = player.position + offset;
        }
        /*
        if (target != null)
        {
            var eulerRot = Quaternion.Euler(rotation, 0f, 0f);
            transform.rotation = eulerRot;
            Vector3 pos = target.localPosition + offset;
            Vector3 sPos = Vector3.Lerp(transform.localPosition, pos, Smooth * Time.deltaTime);
            pos.z += camHeight;
            transform.localPosition = sPos;
        }
        */
    }
    public void setTarget(Transform target)
    {
        player = target;
    }
}
