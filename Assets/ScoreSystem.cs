﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
using System.Linq;

public class ScoreSystem : NetworkBehaviour
{
    public static ScoreSystem instance = null;
    [SyncVar] public float time;
    public float limitTime;
    [SerializeField] public List<GameObject> playerScoreLists = new List<GameObject>();
    public static int highScore = 0;
    [SerializeField] public int getHighScore = 0;
    [SerializeField] public TMP_Text[] scoreLists = new TMP_Text[6];
    [SerializeField] private CheckWin check;
    [SerializeField] NetworkRoomManager managerExt;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        if (!isServer) return;
        GetAllPlayerScore();
    }
    public void Update()
    {

        if (!isServer) return;
        FindNetworkManager();
        SetHighScore();
        CheckAllPlayerScore();
        TimeCounter();
    }

    #region Function
    public void GetAllPlayerScore()
    {
        check = GetComponent<CheckWin>();
    }
    public void FindNetworkManager()
    {
        if (managerExt == null)
        {
            Debug.Log("Nulll");
            managerExt = GameObject.FindWithTag("NetworkManager").GetComponent<NetworkRoomManager>();
        }
    }

    public void SetHighScore()
    {
        getHighScore = highScore;
    }

    public void TimeCounter()
    {
        if (time <= limitTime)
        {
            time -= Time.deltaTime;
        }
        if (time <= 0)
        {
            check.condition = true;
            playerScoreLists.Clear();
        }
    }
    public void CheckAllPlayerScore()
    {
        if (playerScoreLists.Count > 0)
        {
            for (int i = 0; i < playerScoreLists.Count; i++)
            {
                if (playerScoreLists[i].GetComponent<PlayerManager>() == null)
                {
                    scoreLists[i].text = string.Empty;
                }
                scoreLists[i].text = $"{playerScoreLists[i].GetComponent<PlayerManager>().name} : {playerScoreLists[i].GetComponent<PlayerManager>().myScore}";
                if (playerScoreLists[i].GetComponent<PlayerManager>().myScore > highScore)
                {
                    highScore = playerScoreLists[i].GetComponent<PlayerManager>().myScore;
                    playerScoreLists[i].GetComponent<PlayerManager>().winStatus = true;
                }
                else
                {
                    playerScoreLists[i].GetComponent<PlayerManager>().winStatus = false;
                }
                /*
                if (i == playerScoreLists.Count)
                {
                    Debug.Log("Updated Player");
                    i = 0;
                }
                */
            }
        }
        else
        {
            Debug.Log($"SERVER MESSAGE : NO PLAYER ADD INTO SCORE");
        }
    }

    public void ResetValue()
    {
        if (NetworkManager.IsSceneActive("Scene_Room"))
        {
            Debug.Log("RESET VALUE");
            highScore = 0;
        }
    }
    #endregion
}
