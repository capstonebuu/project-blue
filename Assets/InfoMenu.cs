﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class InfoMenu : MonoBehaviour
{
    [SerializeField] private GameObject title;
    [SerializeField] private GameObject buttonInfo;
    [SerializeField] private GameObject buttonStartGame;
    [SerializeField] private GameObject howtoPlay;
    public void gotoHowtoPlay()
    {
        title.SetActive(false);
        buttonInfo.SetActive(false);
        buttonStartGame.SetActive(false);
        howtoPlay.SetActive(true);
    }

    public void gotoMenu()
    {
        title.SetActive(true);
        buttonInfo.SetActive(true);
        buttonStartGame.SetActive(true);
        howtoPlay.SetActive(false);
    }
}
