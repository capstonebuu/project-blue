﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
public class PlayerUserInterface : NetworkBehaviour
{
    [SerializeField] private TMP_Text myScoreText = null;
    private void Update()
    {
        if (myScoreText == null)
        {
            myScoreText = GameObject.Find("Score Text").GetComponent<TMP_Text>();
        }
        if (isLocalPlayer)
        {
            PlayerManager manager = GetComponent<PlayerManager>();
            myScoreText.text = manager.myScore.ToString();
        }
    }
    private void OnDestroy()
    {
        myScoreText = null;
    }
}
