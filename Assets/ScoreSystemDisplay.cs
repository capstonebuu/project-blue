﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Mirror;
public class ScoreSystemDisplay : NetworkBehaviour
{
    [SerializeField] private ScoreSystem scoreSystem;
    [SerializeField] private TMP_Text timeText;

    private void Update()
    {
        timeText.text = ((int)scoreSystem.time).ToString();
    }
}
