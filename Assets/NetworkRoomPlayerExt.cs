﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class NetworkRoomPlayerExt : NetworkRoomPlayer
{
    //Custom GUI by using old version
    public GUISkin mySkin;
    public GUISkin otherSkin;
    public GUIStyle myStyle = new GUIStyle();
    public float startposX, startposY, startW, startH;
    private float x, y, w, h;
    [SerializeField] Animator camEvents;
    private void Update()
    {
        OnChangeValue();
        if (camEvents == null)
        {
            camEvents = GameObject.FindWithTag("MainCamera").GetComponent<Animator>();
        }
    }

    private void OnChangeValue()
    {
        x = startposX;
        y = startposY;
        w = startW;
        h = startH;
    }
    public override void OnGUI()
    {
        GUI.skin = mySkin;
        if (!showRoomGUI)
            return;
        NetworkRoomManager room = NetworkManager.singleton as NetworkRoomManager;
        if (room)
        {
            if (!NetworkManager.IsSceneActive(room.RoomScene))
            {
                return;
            }
            DrawPlayer();
            DrawPlayerReadyButton();
            DrawPlayerExitButton();
        }
    }


    public void DrawPlayer()
    {
        GUI.skin = mySkin;
        GUILayout.BeginArea(new Rect((Screen.width / 2) - x, Screen.height / 4 + (index * 50), 500, 400));
        if (readyToBegin)
        {

            GUILayout.Label($"Player {index + 1} : Ready");
        }
        else
        {
            GUILayout.Label($"Player {index + 1} : Not Ready");
        }
        GUILayout.EndArea();
    }

    public void DrawPlayerReadyButton()
    {
        if (NetworkClient.active && isLocalPlayer)
        {
            GUILayout.BeginArea(new Rect(Screen.width / 2 - x + 400, Screen.height / 4 + (index * 50), 600, 500));
            if (readyToBegin)
            {
                if (GUILayout.Button("Cancel", GUILayout.Width(100), GUILayout.Height(45)))
                {
                    CmdChangeReadyState(false);
                }
            }
            else
            {
                if (GUILayout.Button("Ready", GUILayout.Width(100), GUILayout.Height(45)))
                {
                    CmdChangeReadyState(true);
                }
            }
            GUILayout.EndArea();
        }
    }

    public void DrawPlayerExitButton()
    {
        GUILayout.BeginArea(new Rect(x + 1350, y + 160 + (index * 100), 600, 500));
        if (((isServer && index > 0) || isServerOnly) && GUILayout.Button("Remove", GUILayout.Width(200), GUILayout.Height(75)))
        {
            GetComponent<NetworkIdentity>().connectionToClient.Disconnect();
        }
        GUILayout.EndArea();
        if (NetworkClient.active && isLocalPlayer)
        {
            GUILayout.BeginArea(new Rect(Screen.width / 2 - 100, Screen.height - 150, 600, 500));
            if (GUILayout.Button("Exit Room", GUILayout.Width(300), GUILayout.Height(100)))
            {
                NetworkRoomManager room = NetworkManager.singleton as NetworkRoomManager;
                if (NetworkServer.active && NetworkClient.isConnected)
                {
                    room.StopHost();
                }
                else if (NetworkClient.isConnected)
                {
                    room.StopClient();
                }
            }
            GUILayout.EndArea();
        }
    }

    public override void OnStopClient()
    {
        if (camEvents != null)
        {
            camEvents.SetBool("isTouchToStart", false);
        }
    }

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];

        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }

}
