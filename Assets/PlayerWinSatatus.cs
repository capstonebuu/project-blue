﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
public class PlayerWinSatatus : NetworkBehaviour
{
    [SerializeField] public Text status;
    [SyncVar] public string statusText = string.Empty;
    void Update()
    {
        if (ScoreSystem.instance.playerScoreLists.Count > 0 && statusText == string.Empty)
        {
            for (int i = 0; i < ScoreSystem.instance.playerScoreLists.Count; i++)
            {
                if (ScoreSystem.instance.playerScoreLists[i].GetComponent<PlayerManager>().myScore == ScoreSystem.highScore)
                {
                    statusText = "You Win";
                    status.text = statusText;
                }
                else
                {
                    statusText = "You Lose";
                    status.text = statusText;
                }
            }
        }

    }
}
