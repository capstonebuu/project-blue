[33mcommit 0d8ed456dc66e0d27f9b407537adbc7e1c9c1996[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Merge: 6f09ebd9 f7675083
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Wed Mar 10 12:54:06 2021 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 6f09ebd9cbafd6e158b941d1317669af43325e6c[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Wed Mar 10 12:52:41 2021 +0700

    lease enter the commit message for your changes. Lines starting

[33mcommit f76750834c04417514c1a5267b3afa9ebdbd0092[m[33m ([m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Mar 9 19:09:08 2021 +0700

    Project - optimize game fps and reduce texture usage change lighting from realtime render to baked for reduce gpu and cpu usage and rescale UI resolution form any device

[33mcommit c064f4e00a55f82a25e85db638515ee8af5675c5[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Mar 9 13:58:38 2021 +0700

    Network - fixed room player doesn't display correct

[33mcommit 29d1b5385aab2c07e94150ca639c99ca5b578f17[m
Merge: 8b16d68c 77777ce3
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Mar 9 12:35:38 2021 +0700

    Project - Merge branch 'master'

[33mcommit 8b16d68cb2d60573fa0a719b7334ae096c8465b5[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Mar 9 12:34:35 2021 +0700

    NetworkManger - fixed player can't leave room next test

[33mcommit 77777ce3792b050e2ee0fdebc1eef2de27b29f2e[m
Merge: 52c53d6a 14868c72
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Fri Mar 5 19:53:05 2021 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 52c53d6a72d291e0599bd10fb2f4670889f449f9[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Fri Mar 5 19:52:56 2021 +0700

    Map - Map1 Lab add prefab

[33mcommit 14868c72c867b642f04f2edfb8afd57e0e97dfaf[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Mar 5 17:32:41 2021 +0700

    Scene Title - added more animation when start game

[33mcommit 25b896cf6cf4952157cf35b97324a3ec2b801cec[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Mar 5 15:22:49 2021 +0700

    Scene Title - added animation when start game

[33mcommit d96a5517bf00665e26222f245cb51fe1b228fdf6[m
Merge: b9fe2ce8 ba7a5a70
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Mar 5 00:11:21 2021 +0700

    Project - Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit b9fe2ce8925f6a87c9cd04b599d1a35f0f048567[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Mar 5 00:09:53 2021 +0700

    Scene Title - Add Quality of life in scene

[33mcommit ba7a5a706bb28fe996c562b2741f54eaa2ee7b02[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Thu Mar 4 22:19:52 2021 +0700

    Item - increase size item and fixed bug  block not destroy after spawn

[33mcommit 839941fb686a210556567c9a855d2eef9c426f22[m
Merge: 5208119c 041b1dca
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Mar 3 18:02:49 2021 +0700

    Scene - fixed merge from scene_test.unity

[33mcommit 5208119c45aa6d3bd347bb858d4d53d35e38c9a6[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Mar 3 17:59:53 2021 +0700

    Project - making GameController find player when they join a game and assign them into Score System

[33mcommit 041b1dcada8a48891d9bf522b14a75ed5d66438f[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Wed Mar 3 15:37:14 2021 +0700

    Scene -Add Demo scifi

[33mcommit 888355320a2bb8a96b09f7c7d0cc5342d08e2bb8[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Wed Mar 3 15:05:37 2021 +0700

    item - added item speedup

[33mcommit 7e312ce77dd41b8e0d7dfd02cbeaf35faa142224[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Wed Mar 3 14:26:27 2021 +0700

    add potion ui

[33mcommit 352589a5b693f2d61c251c944242e6431af09e3e[m
Merge: fb92e35c 244d0b88
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Mar 3 14:18:08 2021 +0700

    Project - Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit fb92e35c1e9322db9af46c56435e4a5f2fd698e3[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Mar 3 14:17:57 2021 +0700

    Project - fixed size of map make it to flat and fix client player can't pick up score

[33mcommit 244d0b88890b15137820cce9f70def6ebd2d1cf3[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Tue Mar 2 14:09:26 2021 +0700

    Prefab - add joybutton to interactable prefab still have bug on interactable button when player collider enter interact prefab other player can use too

[33mcommit 30f116ca3bc0337602d3b58eec2c968f9d8f75cc[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Tue Mar 2 01:35:12 2021 +0700

    Item - added block item and change spawn item position to the ground

[33mcommit a73df30eb03055fba5671bd535164638fc688378[m
Merge: d1abaced 4c3f8122
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Mon Mar 1 21:21:55 2021 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit d1abaced87be91ec147ee77f0947e5f3be49ecf3[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Mon Mar 1 21:21:43 2021 +0700

    Map - Fixed map4 sidewalk player can walk

[33mcommit 4c3f8122f4cf48e17cd8259d44cead0dba5b4178[m
Merge: 1657852e e57d86e3
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Mar 1 19:26:24 2021 +0700

    Project - Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 1657852e808ca2f70aff489a9bddc75b6fdfb2bc[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Mar 1 19:26:10 2021 +0700

    Title Scene - added gui for lobby and play

[33mcommit e57d86e3c10e0318f82cb64b637985ef084d225b[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Wed Feb 24 22:52:51 2021 +0700

    Map - Edit map4 town add sidewalk and fix road

[33mcommit a8df17e5d1f31b3a18b045a4721dcd8b56557d2e[m
Merge: 408d0f98 2c02c1fb
Author: Pudi <60160079@go.buu.ac.th>
Date:   Wed Feb 24 14:57:37 2021 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 408d0f98307e678973370acc68c7250d6efc0a5c[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Wed Feb 24 14:57:31 2021 +0700

    Item - Added block item

[33mcommit 2c02c1fb58db17b274b8a790e54341412efd17df[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Feb 24 13:38:14 2021 +0700

    Project - merge git

[33mcommit 7d68169d22ec1d91fd74c9a38710959813356780[m
Merge: 6d428eee 6c5c55e9
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Feb 24 13:36:41 2021 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 6d428eeed62e48c460652885212995d847862ecf[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Feb 24 13:36:25 2021 +0700

    Project - commit before pull

[33mcommit 6c5c55e9267efaa728f1438c042b620dfd58e545[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Mon Feb 22 21:30:19 2021 +0700

    Item - add color to item when spawn player can see it

[33mcommit ac5efabf9e1f624ce3d178c2911a6ff3d8f9916c[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Mon Feb 22 21:03:55 2021 +0700

    Item - Fixed item respawn show onclient and added color

[33mcommit 53ccacc7fee1b42c26bf2e593484ab497ff57955[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Feb 21 21:13:48 2021 +0700

    Scene - replace with sample map

[33mcommit d08b5776eca3f3f0f3507b8bf966a2f118881293[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Feb 21 20:56:28 2021 +0700

    Project - fixed

[33mcommit 858f9fc71dbce4361042ec7b2a4bea94463c5d0e[m
Merge: 554c2299 c2c9b3d7
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Feb 21 17:47:34 2021 +0700

    Project - fixed merge

[33mcommit 554c22990af13478eb16802c200b1e89825993a6[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Feb 21 16:32:24 2021 +0700

    Lobby - tried to make team system

[33mcommit c2c9b3d7ebed50507f4b64f0633bf6756ac8b75a[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Sun Feb 21 16:30:15 2021 +0700

    Item - Fixed SpawnItem

[33mcommit 14b23d99e16d9e62d67bb505428ca818b1d93912[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Sat Feb 20 21:06:48 2021 +0700

    Map - create new map town

[33mcommit 1c7cb8a3abce2a6bc88905a360983986514a3ec6[m
Merge: fc335979 f496b38f
Author: Pudi <60160079@go.buu.ac.th>
Date:   Thu Feb 18 14:55:54 2021 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit f84cf62fd1b68b72c6579b3bb482affde2c0235d[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Feb 18 13:59:06 2021 +0700

    Title Scene - add new building

[33mcommit fc335979662fc3a2731cd88b21eecc996092a6d1[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Thu Feb 18 13:45:46 2021 +0700

    Interactable - Teleport Pad implement to interactable button when enter area

[33mcommit f496b38fe534a595ef55777b13b6b7cc21052a5f[m
Merge: 5d65f12d 2fe51a79
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Feb 18 13:29:28 2021 +0700

    Project - fixed merging

[33mcommit 5d65f12ddb068c0e19bdcf6834b5aa9d23120152[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Feb 17 18:17:50 2021 +0700

    Project - added Universal Render Pipeline for make game looking good so good and Shader Graph

[33mcommit acf98b2f67887deab88ba4f3f7df1df23470abe4[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Wed Feb 17 17:44:52 2021 +0700

    Prefab - add minimap for vertical door and water pipe

[33mcommit 2fe51a794e19ed73a20daa9cd9eaeef17e20805e[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Wed Feb 17 12:00:06 2021 +0700

    Scene delete item in scene / todo add sign to prefab for minimap and add material or color to prefab

[33mcommit 550aa16e96a427a3e62dbe344df08e2998fad310[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Wed Feb 17 11:55:55 2021 +0700

    Item - Respawn item when player pickup after 5 second / try todo random spawn but not work so back to basic :(

[33mcommit 2ff621107eee3766b8ad31f9bb9156d6d6e085e2[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Mon Feb 15 10:41:29 2021 +0700

    Item - Can respawn item when player use item /todo do the rest item now can respone only 1 item

[33mcommit 4b88b735731461528f916cfa5de3a9fcc6535ffc[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Feb 12 18:20:26 2021 +0700

    Project - add mini map

[33mcommit 4a40b3d116cf3b36e5854016f9a017e32fc7d5bb[m
Merge: 63ffeee4 4465b5a7
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Feb 12 16:39:32 2021 +0700

    Project - Fixed confilct

[33mcommit 63ffeee452904f92523571333e589142c0106c8c[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Feb 12 15:34:30 2021 +0700

    Scene - add ui

[33mcommit 1310b64151098599634b6ac8f728c91c3cf2f2c3[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Feb 11 23:15:55 2021 +0700

    Network - Fixed ui when start game

[33mcommit 4465b5a7d51f302c6ea704d72e288f13d7736951[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Thu Feb 11 11:26:08 2021 +0700

    Add External asset polygoncity

[33mcommit 0f9cbca345a88ba780dd7d5c000f1e046233844d[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Thu Feb 11 10:09:52 2021 +0700

    Player - Fixed bug when client use item spawn on host and fixed bug item spawn twice and not destroy / todo add some skin effect and respawn when player use

[33mcommit eba6ddbe6ca574c8fd14d66b14c3de6f8a8aa737[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Wed Feb 10 12:37:14 2021 +0700

     Player - Fixed spawn item when use and item working when it spawn / todo bug when client use item  item will spawn on host location

[33mcommit ecf9bd2d428ed7b44292777a848f6bf266cab253[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Wed Feb 10 07:03:16 2021 +0700

     Player - Fixed pickup script v.2 / todo  spawn item when use and make item working when it spawn

[33mcommit 6199f8250a7654bc7aeb384699e906b57173a1ed[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Tue Feb 9 15:55:34 2021 +0700

    Player - Fixed playerpickup and useitem script / todo fix item

[33mcommit b8eb979091f63e072b7cde426a981f3aba0a6e31[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Feb 9 15:21:14 2021 +0700

    Project - added debug scene

[33mcommit 745dcac441d111c8f2f5325ece8ce0f7a474c06c[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Feb 8 20:41:59 2021 +0700

    Project - recreate networkmanager lobby and ui

[33mcommit fd161e1601c44be0fa555f4774bce9b80dda1b88[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Feb 8 17:32:23 2021 +0700

    Project - added show icon script for editor mode

[33mcommit fe8c497cbbd79a17bb8d9804354ef5e586a1bda0[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Feb 8 16:41:32 2021 +0700

    Network - add NetworkManagerLobby scripts

[33mcommit 39a9a42de652055796d9481294f44cc7b1ddd3a5[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Feb 8 16:34:50 2021 +0700

    Network - add NetworkRoomPlayerLobby to manage player

[33mcommit 859868ecb67c526b0e3e6110f46f786a55c22398[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Feb 8 13:45:34 2021 +0700

    Project - fixed confilct

[33mcommit 651f65c4eb388d9f628ccedf2aaabce4218f45d0[m
Merge: ecf17896 c4aaab6c
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Feb 8 13:37:21 2021 +0700

    Project - pull and merge

[33mcommit ecf178962370c82d3dc4f3decf8bb0cbe7977f92[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Feb 8 13:36:43 2021 +0700

    Project - Commit before pull

[33mcommit c4aaab6c7d77179ab4e501d1c6ec648792b73fba[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Mon Feb 8 13:31:17 2021 +0700

    Player - Fixed bug on multiplayer when pickup todo fix usetiem

[33mcommit 0aa2c6ac94aace88d3177c43a3ae83441ea09015[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Thu Feb 4 19:53:07 2021 +0700

    Player - Item when used item will remove from slot  todo have bug on multiplayer when player pick item will added to both player

[33mcommit 2bd3b2e23d83528730d2bbf78b3d797627280a5c[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Thu Feb 4 09:32:24 2021 +0700

    Player - Can Added item and show on interface todo use item and remove from slots

[33mcommit 8b81681328e7e39847d35fb42c4f2713f1c64f84[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Wed Feb 3 10:04:28 2021 +0700

    Player - add script and test pickup test item

[33mcommit e6489edd2bf9c683b6551ec3100d45807d2ba1e2[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Tue Jan 26 14:45:45 2021 +0700

    Script - rename waterpipe and teleport scirpt

[33mcommit 00503bf2492164c9d351a4803f6ae4c33ea061c6[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Tue Jan 26 14:37:34 2021 +0700

    Obstacle - Slowpad fixed speed

[33mcommit e8ec7974b359bf3d2a263f9b09496425e949d5d9[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Tue Jan 26 14:32:02 2021 +0700

    Script - rename Script slowpad and active slowpad

[33mcommit 92004814763075c4d79a939e254e65a5f20fb954[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Tue Jan 26 14:12:58 2021 +0700

    Obstacle - Fixed bug oilfloor effect clash

[33mcommit 86162535e9bab43e7a4806a95d80cb4dbb5c038f[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Tue Jan 26 12:56:11 2021 +0700

    Scene - adjust scene_test added skin for oilfloor and fixed teleport skin

[33mcommit 5a68d70ff3de94f3bb53a1748809e2c4a6c42482[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Mon Jan 25 10:00:46 2021 +0700

    Obstacle - Teleport Fixed effect and fixed player stun animation

[33mcommit 680d9e3b0c922854bd560809f4d958d3f76a5269[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Sun Jan 24 21:57:11 2021 +0700

    Obstacle - add effect Slow Pad( oil floor) todo need to fix teleport effect for multiplayer

[33mcommit bd41ca30f56001671d3b6c7546ee6a556f6176bd[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Sat Jan 23 14:26:44 2021 +0700

    Obstacle - fixed bug animation when use teleport and add effect

[33mcommit f801e173220cd1de387a73870b6c6298b2fb6cb1[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Jan 22 16:02:57 2021 +0700

    Project - commit before reimport asset to fixed Error in Vscode

[33mcommit 896e0db75130af6f549f53cc1adcae001420902a[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Fri Jan 22 15:22:39 2021 +0700

    Attack - added stun animation when player got attacked

[33mcommit 618e9972527123d458d7c84f28dc080c9365c6d6[m
Merge: 715c8566 9e7c4795
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Jan 22 14:06:22 2021 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 715c85661cf300b068389ef10c0f1329dddcdfb6[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Jan 22 14:06:05 2021 +0700

    Project - commit before pull

[33mcommit 9e7c47951babbc8db6421d375322977917a50494[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Thu Jan 21 15:44:49 2021 +0700

    Obstacle - Water Pipe added skin for valve

[33mcommit d15256b63017cf376cfb2fb312fae68c61c35440[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Thu Jan 21 13:01:31 2021 +0700

    Obstacle - Water Pipe added effect

[33mcommit 087eeca690c99fbaa71593708527058bb8ce9a0b[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Wed Jan 20 13:35:15 2021 +0700

    Obstacle - Fixed waterpipe bug on multiplayer and change test scence

[33mcommit 49201d8a0628382454b5fd161b4dc829a36bb6d6[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 18 16:37:48 2021 +0700

    Project - fixed lobby

[33mcommit 5e95825ac47c2fd4870276de6a0814970b3f1523[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 18 14:47:54 2021 +0700

    Lobby - Test Login and Lobby Systems

[33mcommit 36e6de35d8f2486595e8226645204987b54cebca[m
Merge: 4b4cf0f1 558752b3
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 18 14:37:23 2021 +0700

    Project - Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 4b4cf0f13f2ac733aace5b72d3b4253c13c63426[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 18 14:37:12 2021 +0700

    Project - Commit before pull

[33mcommit 558752b38a8fcbe30e7ad22c66e818211361d569[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Fri Jan 15 13:57:09 2021 +0700

    Player -fixed animation run can show on multiplayer

[33mcommit 875bb9b9c7a39a30c9014007b4d7ff8b1d4260f4[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Fri Jan 15 13:27:56 2021 +0700

    Obstacle - fixed teleport can travel two way

[33mcommit c44b5f67e992e49a952a620a0a5457749820280d[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Fri Jan 15 13:11:28 2021 +0700

    Obstacle - added teleport skin and fixed teleport target position

[33mcommit 110d83e9bed56a886a5f1debb77c03cbfb6f04f4[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Thu Jan 14 15:44:44 2021 +0700

    Obstacle - added skin for slide Door (Horizontal) and fixed scence test

[33mcommit b594bea23cdb49be75b0e86021219df50fd5c14e[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Wed Jan 13 16:12:06 2021 +0700

    Attack - added attack animation

[33mcommit cdd0096c400cf991191caf00e2dfde4e700a5c7f[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Wed Jan 13 13:22:08 2021 +0700

    Player - Added run and idle animation

[33mcommit cea691e2db7e50e1542bc36991ac736d1643b4e7[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Tue Jan 12 14:42:56 2021 +0700

    Obstacle - fixed slide door close position and add visualbox and room to test

[33mcommit 645ae7073adf47d2c2f288262db472ec51080e67[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Tue Jan 12 13:19:06 2021 +0700

    Attack - fix after hit target disable movement for 3 seconds

[33mcommit 7b18987c79d36f2b569ff4c05e0c1dc4eb1aa6b2[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Mon Jan 11 17:13:27 2021 +0700

    WaterPipe - Change code to client server still have bug on client when active after 5 second it not deactive

[33mcommit 153f64424b8407ed8a9d00c2e4d9dff6e1de6471[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 11 11:36:23 2021 +0700

    OilSlow - fixed speed when player enter and exit

[33mcommit 7cf3ea8a931f1f44ca95d9dea88a218711a8f654[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 11 11:25:24 2021 +0700

    TeleportPad - add teleportTarget to child

[33mcommit d264122248c9958b234acbf9e52f74659d090e60[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 11 01:01:31 2021 +0700

    Oil - fixed null exception and SyncVar hook

[33mcommit 26a6267da8f3ec5205e16ad6d115b78568387d28[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 11 00:11:35 2021 +0700

    Oil - Sync change speed client and server

[33mcommit edd4184af26b8138a1bbc36be653647691583494[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Jan 7 17:32:54 2021 +0700

    Oil - Sync variable to slow player

[33mcommit a317b44411c95e3aa3d19d5b00297a0afe66142e[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 4 14:40:50 2021 +0700

    Wall - fixed rotation of player when jumpping

[33mcommit 9f6794d405dfa0a4e331bca7b087923c8f57594f[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 4 13:22:28 2021 +0700

    Door - applied prefabs

[33mcommit 0d730551d9402d1a16256eb85dc9756143812ca2[m
Merge: c5a63b48 d77ed80a
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 4 13:20:51 2021 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit c5a63b48cc807f89db801b84c9bf2c11cf48a145[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Jan 4 13:20:38 2021 +0700

    Door - Sync door Event

[33mcommit d77ed80a2c238fecfa14c0a4f89d65674871d9d1[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Sun Jan 3 22:17:19 2021 +0700

    Attack - commit before pull still not work

[33mcommit 1580035585f6901fea1626b3b7136711b7bc0c4a[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Jan 3 14:26:38 2021 +0700

    Player - Fixed camera follow player

[33mcommit 88dab7babe661a13bedd79c1e3b04f73801ae525[m
Merge: 2735c4f8 b816fb2e
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Jan 3 13:50:14 2021 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 2735c4f8bb0bc76f1a8a67d87fe81208e081dbd8[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Jan 3 13:50:06 2021 +0700

    Project - commit before pull

[33mcommit b816fb2e39622f747469aabbe6f20550ebbe43da[m
Merge: 2cfb5f32 c53bbba8
Author: Pudi <60160079@go.buu.ac.th>
Date:   Tue Dec 29 12:24:21 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 2cfb5f321987483d00a5cbf8ec0f00b7459372f6[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Tue Dec 29 12:23:55 2020 +0700

    Attack - implement code to client server can attack and wait for fix to hit enemy

[33mcommit c53bbba84c5f4185877d380617880d890199a622[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Dec 28 10:48:58 2020 +0700

    Interact Door - update visual activate box

[33mcommit 84a38ee5d75d804bf158902a70bb45e6e81c7315[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Dec 28 10:22:10 2020 +0700

    Project - commit before pull

[33mcommit 72aff67202c1233bdf655cf930bd08c76f61ded9[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Dec 27 17:13:17 2020 +0700

    Project - Completed for now next fixed Camera

[33mcommit 839dbddfc3d53f0f6fd2b6ec93ab0bf5ee8bb746[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Dec 27 16:25:22 2020 +0700

    Project - Basic element completed

[33mcommit 872ebe19c90def9197a09f729a16ec4f878e226d[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Dec 27 15:19:02 2020 +0700

    Network - make transfer player completed next auth each clients

[33mcommit 997e9f895eef40d1e0abbb1140e5760e5b9da15c[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Dec 27 12:10:12 2020 +0700

    Player - need to fixed bugs On client connected

[33mcommit f2a1c0de0af6e42ce99cc352ed72b9111b4ace9b[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Sun Dec 27 10:58:25 2020 +0700

    Network - make Transfer player into game scene

[33mcommit 018c84320b09b0173ac93532ff88c4cd9b65e673[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 23 15:55:56 2020 +0700

    Network - create Lobby Rooms

[33mcommit ae64d68e8ad5d20f82827ab373bb200fdb30e313[m
Merge: b704dda6 777dea2d
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Dec 22 16:04:37 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit b704dda615cb349debc6ac14e19f2a28a12f978a[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Dec 22 16:04:24 2020 +0700

    Networking - Make Lobby

[33mcommit 777dea2d1e4669c3ac0f6e15a00d4f21240d116a[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Tue Dec 22 14:55:15 2020 +0700

    Attack - modified MainObjective can steal from another player

[33mcommit a18ef4a04950b8f5f8150bb5f02c388d3db3c76f[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Dec 22 14:36:14 2020 +0700

    Camera - fixed error

[33mcommit d7f18c10ad0b8f769aa4b49d0b652bfc0bac8f89[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Dec 22 14:31:14 2020 +0700

    Player - tried to make camera on each player but failed

[33mcommit 735e04186456e71653de92e22278fa34cb4f9c6b[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Dec 22 12:21:35 2020 +0700

    Scene - save scene

[33mcommit 7dd9a9dabe8be5292390d56ad9109fb188ccc61c[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Dec 22 12:21:11 2020 +0700

    Dummy - Make Dummy player

[33mcommit 1aaa1d3ee3444496b2f1853da1d7a1a6df52da70[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Dec 22 10:16:59 2020 +0700

    Player - remove player from scene

[33mcommit e548927ce2dbef74e11af04fc184b81814d1acd5[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Dec 22 10:13:37 2020 +0700

    Project - commit after pull

[33mcommit 63be5a8a851ea5f53513f3981f07d5c77d441ea2[m
Merge: 981a65ac 184bea5b
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Dec 22 10:11:09 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 981a65acb7a5e7aff332f0c65137a57007eb6e36[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Tue Dec 22 10:11:03 2020 +0700

    Project - Commit before pull

[33mcommit 184bea5b2d4c423018a330b3e84e3d180ed22fc6[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Sat Dec 19 09:06:33 2020 +0700

    Attack system - test with using mirror  attack puppet

[33mcommit 700f52e748af5572d91a282f6ff9cf98e4d62a8f[m
Merge: f43ae251 bfbfe5c5
Author: Pudi <60160079@go.buu.ac.th>
Date:   Fri Dec 18 21:05:42 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit f43ae251bdc36717ec7938eeb293b9a9f9b4a1b2[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Fri Dec 18 21:05:25 2020 +0700

    Attack sysytem - test attack

[33mcommit bfbfe5c55a0eb574845f3198ea551ea0c8326d40[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 18 16:19:52 2020 +0700

    Project - Merger project

[33mcommit ef264a4562c75ac0b825ddc512f2958c448951ab[m
Merge: 8a8f5b16 5591132b
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 18 16:18:16 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 8a8f5b166c199982bbf75b9d84e0f9f6daaa7b1d[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 18 16:18:03 2020 +0700

    Project - reversion git

[33mcommit 5591132b2086f18b4097de87b762d7d496519e9f[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 18 14:46:43 2020 +0700

    Scene - added network server test connection

[33mcommit be7ba78ad7ae01cbda5c9dd5fbf5b6be4dfb094e[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 18 13:27:33 2020 +0700

    Camera - fixed find player before spawn cause error

[33mcommit c2b69a242acc88a55efa3ae3ea8db75386a4a1e5[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 18 13:22:58 2020 +0700

    Project - add mirror script template

[33mcommit 615ee22ca01ae2edbeef271aeade2a9b3c3e483a[m
Merge: b0ece98f 964aa66b
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 17 16:39:55 2020 +0700

    Fixed Bug Merge player prefabs

[33mcommit b0ece98f0a288ef6bfab35b4c7b2efd2d762bc23[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 17 16:31:09 2020 +0700

    Player - fixed player prefabs is broken

[33mcommit 4323c758fdccee8ab7c8f48b5144ffb338a34928[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Thu Dec 17 15:26:34 2020 +0700

    Attack - can attack puppet and puppet will set not active

[33mcommit 964aa66bea4b849820c53996e55c36cc07f1cf8a[m
Merge: 225c4831 5b5457d2
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 16 16:39:03 2020 +0700

    Player - Merger player prefabs

[33mcommit 225c483158e1c5116c36aebe8fc09e4615ee6883[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 16 16:38:24 2020 +0700

    Player - Change networking component

[33mcommit 5b5457d2e02e4e36c0a58ecde228a4de3ba23151[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Wed Dec 16 16:36:58 2020 +0700

    Attack system - Added attack scirpt and hit box waiting for server to test

[33mcommit 26b77b349fe6533f52dec091dfbc7227a8c29f17[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 16 14:47:29 2020 +0700

    Project - remove Socket.io add Miror lib

[33mcommit f9e36217aa75d7a93dd97a85ea137ba66d728e45[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Wed Dec 16 14:36:36 2020 +0700

    Obstacle - fixed Oilfloor to have slowactive to trigger

[33mcommit a88acabf19f6927cd01faf8964f61b60bd92703f[m
Merge: 6a60e84d b3c19410
Author: Pudi <60160079@go.buu.ac.th>
Date:   Wed Dec 16 13:31:22 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 6a60e84df02906dda1b005a55236e344b2aa6606[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Wed Dec 16 13:31:05 2020 +0700

    Obstacle - Added waterpipe to prefab

[33mcommit b3c19410e48a1ff7cb4522fee71efaf6c174533d[m
Merge: 958fbc3f 59a65e78
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 16 13:29:15 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 958fbc3fd0a4b90c32adf1247f54b7a0080b2946[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 16 13:29:01 2020 +0700

    Project - commit

[33mcommit 59a65e78fe27bcecc98441594c96c79cecf49962[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Tue Dec 15 19:11:07 2020 +0700

     Obstacle - Added trap water pipie

[33mcommit 18ed756e9ab94f40805f6ccc3b36eee741aaea72[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Tue Dec 15 18:41:17 2020 +0700

    Obstacle - Added OilFloor trap to slow player

[33mcommit dc9aed080500df8e1e56fad9537ad2cb673b6fb7[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Dec 14 16:08:04 2020 +0700

    Project - Added movement communicate

[33mcommit ef1cda5ddc1c5a8eb45fd5d118cb972cce37622e[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Dec 14 14:31:20 2020 +0700

    Project - Update gitignore and add server into project

[33mcommit 5cc1c321bde962aedf23bd80549a8d28eba1fe9e[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Dec 14 14:27:52 2020 +0700

    Networing - added generate id and spawnning player

[33mcommit 9b2b036836cb2f41f96a9eda51541c941fff8028[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Dec 14 11:56:42 2020 +0700

    Project - added Socket.io packet into unity

[33mcommit 6554486a79a78f9fd4a445a93e488c937d8c4b47[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Dec 14 11:37:44 2020 +0700

    Project - Remove mirror network asset

[33mcommit e73e453dd71ede6727870b7c9b9ffe870f470ace[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Mon Dec 14 10:37:16 2020 +0700

    Project - commit before push

[33mcommit 4da3c243f701f434b89957ce33c820b03623f801[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Thu Dec 10 11:03:46 2020 +0700

     Obstacle - added open SingleDoorH type3active script waiting for improve

[33mcommit f41088e2de22b649c80e16c1118144a2e776680f[m
Merge: 39b75406 f2edcb57
Author: 60160239 <60160239@go.buu.ac.th>
Date:   Wed Dec 9 15:56:25 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue into master

[33mcommit 39b75406aa30771eb6d515613c4da463c9638d13[m
Author: 60160239 <60160239@go.buu.ac.th>
Date:   Wed Dec 9 15:56:18 2020 +0700

    Obstacle - Try to improve opening/closing door mechanic

[33mcommit f2edcb57cad00d3aaf9c370c005d7552f4040aca[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 9 14:35:20 2020 +0700

    Project - Remove unused Asset and add Mirror Asset

[33mcommit 54164dd0ab5d81d33b44e7080cf9418e59691604[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 9 12:01:26 2020 +0700

    Project - Try Fixed Crash on ther computer

[33mcommit 12f5f4b0b59c6538adaf36764c47bc745565abe5[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 9 10:47:17 2020 +0700

    Scene - copy sample scene to fixed bug

[33mcommit 1780dbb269b6466c8b185e5982d28a450723e682[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 9 10:40:48 2020 +0700

    Project - Test Push

[33mcommit 38fd099329db30e5dfdfd0219138ee64af48a50d[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Tue Dec 8 16:13:36 2020 +0700

    Obstacle -added if while interact teleport disable movement player

[33mcommit 68bb2318f7b83634d990476f79181deff13fb614[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Tue Dec 8 16:02:30 2020 +0700

    Obstacle - Added TeleportPad and TeleportTarget to prefab

[33mcommit 1d7aa79caf45d036d10ed586d73c4a3586e329ea[m
Author: 60160239 <60160239@go.buu.ac.th>
Date:   Tue Dec 8 15:35:10 2020 +0700

    Project - finally fixed merge conflict

[33mcommit f6d35b9271504cf7330248a7c37d9837b80f959f[m
Merge: aa139856 7d1f89c4
Author: 60160239 <60160239@go.buu.ac.th>
Date:   Tue Dec 8 15:14:11 2020 +0700

    Project - Merge

[33mcommit aa139856c2909b67d8b54410808e7a99024539c0[m
Author: 60160239 <60160239@go.buu.ac.th>
Date:   Tue Dec 8 15:10:27 2020 +0700

    Obstacle - Interactable Door (Vertical)

[33mcommit 7d1f89c418dd14e953d616ff2c5ee60764563c38[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Tue Dec 8 14:43:58 2020 +0700

     Obstacle -add Interactable TeleportPad(Pipe) and script

[33mcommit 86af8c6bc4f6e8a322565088eb038aae19c43ce7[m
Author: 60160239 <60160239@go.buu.ac.th>
Date:   Tue Dec 8 13:36:04 2020 +0700

    Obstacle - Added Interactable Wall (S_1) along with its code and update BasicMovement to use along with Interacting 'Jump'

[33mcommit 42ac9d9778ed4b46c06a5b1e57c54df565e94f55[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 4 16:08:48 2020 +0700

    Project - sure commit

[33mcommit d8c65bea65d1d237c19dd2eb35447ccdedf0d55f[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 4 16:03:53 2020 +0700

    Player - move and rotation with touch input

[33mcommit 6ea6b7f5b3fa83e9e38859e103b829984c08adf6[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 4 14:50:17 2020 +0700

    Game Controller - added win screen componets

[33mcommit a36318d3a94cf4c1908c5a6f85eb61a9341484c1[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 4 14:48:05 2020 +0700

    Win Screen - added win Screen

[33mcommit 2c9f532c80f58825763d4e5585ab74237a998372[m
Author: 60160239 <60160239@go.buu.ac.th>
Date:   Fri Dec 4 14:46:12 2020 +0700

    Objective - updating pick up objective

[33mcommit 5b8c0940ff6dd36d00fdffa13359f7a02b768920[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 4 14:17:20 2020 +0700

    UI Controller - add win Screen

[33mcommit 638593b58c9d8a696a220dd3cc0a6589e46b02ab[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Fri Dec 4 14:01:24 2020 +0700

    Object - Deleted unused spawn point

[33mcommit 0e49a5f57013977e966784aee80a9accca182a0a[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Fri Dec 4 13:53:38 2020 +0700

    Gamecontroller - objectivespawnpoint

[33mcommit a4e45cf8fd0e85fb0b1f81665863de9f04ef5a12[m
Merge: 4ebabb52 3f5bf3df
Author: Pudi <60160079@go.buu.ac.th>
Date:   Fri Dec 4 13:46:03 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 4ebabb52a0dd132a19f5fed75d2d9a73822ec46a[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Fri Dec 4 13:45:42 2020 +0700

    Gamecontroller - checkwin

[33mcommit 3f5bf3df85c0b68f9a0093701b66a121dee2e91f[m
Author: 60160239 <60160239@go.buu.ac.th>
Date:   Fri Dec 4 13:41:11 2020 +0700

    Main Objective - Pick up Obejective updated(scene)

[33mcommit 14008623ae5d40859e333795a478cde743b93326[m
Author: 60160239 <60160239@go.buu.ac.th>
Date:   Fri Dec 4 13:34:43 2020 +0700

    Objective - Drop point update

[33mcommit 865471eda984be87c091aba1334fd3ee8f13d556[m
Author: 60160239 <60160239@go.buu.ac.th>
Date:   Fri Dec 4 12:51:24 2020 +0700

    Main Objective - Pick up Obejective updated

[33mcommit 6a83d963b38572ae9aadd027274bfa1cf08a44f8[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 17:08:12 2020 +0700

    Project - commit sure

[33mcommit fbb93dee90bb26dc2f5ab2a057f2a760ba275984[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 17:07:27 2020 +0700

    MainObjective - fixed can't find carryPoint

[33mcommit 2300c301ef6309f8539f8c7ceb57196a5dcf96d1[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 17:03:15 2020 +0700

    Camera - Fixed Camera can't find game object that tag is <Player>

[33mcommit 84291e55de5be7b8bdba42883a6eb9a6eb8d671a[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 16:54:10 2020 +0700

    Camera - Fixed Null at find game object of player

[33mcommit b5a0cf69d9ae54e8b0e37673b2e8561686daf732[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 16:49:36 2020 +0700

    Camere - Fixed Spawn Player Find target to follow

[33mcommit 55db8cd2d81f73c22d726a7dd3e8dafd6f00c106[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 16:37:24 2020 +0700

    Scripts - Remove unsed scripts

[33mcommit 4bbb9b6b2aff33f696cc156d139024c4904b1ef1[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 16:31:59 2020 +0700

    Game Controller - fixed pick up iteam

[33mcommit 2d8744fceac4312f4321fb542b5ef387fbbbb3a5[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 16:14:03 2020 +0700

    Map - forget saved

[33mcommit 336b19bc3800c2ebb3224439b0bad398a0be1ee2[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 16:08:17 2020 +0700

    UI Controller - fixed can't reference joy variable to player

[33mcommit 33f4ee1bc195f28f128b2b2049bb11a19b017e86[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 15:53:14 2020 +0700

    Project - Sorted all files in assets

[33mcommit 98b35625a75ee56ba8fa37508ddd31d8511e32bf[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 15:43:20 2020 +0700

    UI Controller - added ui controller

[33mcommit 67437cb285db1a034b04c2005c64a4c3225fea3f[m
Merge: 65d1cf5a 4e4e3891
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 15:42:49 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 4e4e3891a999167f0f771e00926a2272c8f3cc59[m
Author: Chanachon Danpreeda <60160239@go.buu.ac.th>
Date:   Thu Dec 3 15:41:33 2020 +0700

    Objective - Simple Pick up Objective and Drop Objective

[33mcommit 65d1cf5ab9d5695cae09fc61f3fb2408a09f7196[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 15:36:10 2020 +0700

    UI Controller - added Interact button

[33mcommit 97e18c0ce0a30a7bd87a2331ae873ba8a08fc13b[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 15:14:44 2020 +0700

    Camera - added to prefabs

[33mcommit 57628d335cb0b4e5454d667243951c622d02c759[m
Merge: 41a54267 cdd90518
Author: Pudi <60160079@go.buu.ac.th>
Date:   Thu Dec 3 14:55:57 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 41a5426760b40190c5be43079c493b314b6ed6ef[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Thu Dec 3 14:55:42 2020 +0700

    fix map2 edit prefab scale

[33mcommit cdd90518e8f0f1457543e68703022142752961d8[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 14:33:16 2020 +0700

    Map - Rename map and redesign map 1

[33mcommit d34526a4c0c703758fbe5237582f43eafbe1c0a0[m
Merge: 9d089cf5 2b670737
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 14:21:12 2020 +0700

    Merge branch 'master' of https://gitlab.com/capstonebuu/project-blue

[33mcommit 9d089cf55c98f5d033ee9e40d0da8ba19cb8b5a9[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 14:21:03 2020 +0700

    Player - Change movement a bit

[33mcommit 2b670737c51ab449e3729c4a2361cf950ae8aeb3[m
Author: Pudi <60160079@go.buu.ac.th>
Date:   Thu Dec 3 14:19:41 2020 +0700

    fix map2 roof top  edit scale y and position

[33mcommit 9eb0c23e55c1f64fcd51b6415b47a54c877c110b[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 13:53:37 2020 +0700

    Camera - Change view angle to normal

[33mcommit b84d1dba75d1f6ffd2e76d815d65a0e5fda4b349[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 13:30:19 2020 +0700

    Player - sync movement with touch input

[33mcommit 7facf87ec30f5a35bfc44ebde29d6e182a5c8228[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 13:15:58 2020 +0700

    Player - make rotation with Touch

[33mcommit ee2a3c79291f6bd04595fcb082920ec5af02efa6[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 12:17:35 2020 +0700

    Camera - make following player with out rotation

[33mcommit 2b9966cbf363be3fd29499520a63126596c4b2fa[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Thu Dec 3 11:34:26 2020 +0700

    Player Movement - Fixed Camera Following player

[33mcommit 1ac45bc5a31343003ec27b43cb64020cde1d0d38[m
Author: Worapon Tangsiripat <60160079@go.buu.ac.th>
Date:   Thu Dec 3 06:00:02 2020 +0700

    [Player1SpawnPoint],[ObjectiveSpawnPoint] : add playerspawnpoint and objectivespawnpoint

[33mcommit 02659e4e5200a6bedd6e621b2ca408bd9505b216[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 2 16:50:29 2020 +0700

    Project - Setting For Android

[33mcommit 7c20ddf60d957ff380fa1dfde2ed06b306897a07[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 2 16:01:40 2020 +0700

    UIController - Added JoyStick and Attach New Movement

[33mcommit c4074fb35bea29661b27489eae1b9b6aca45805e[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 2 15:29:19 2020 +0700

    Project - Add Simple Map and Player

[33mcommit b2d24e94e7fb3001067b5a27c5a49abb161b1c0d[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 2 14:47:44 2020 +0700

    Scene - Change Sample Scene map

[33mcommit d8ca2858d0b91e70523fa4b4f3accb598cf3cdfb[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 2 14:46:01 2020 +0700

    Project - Remove network Feature and Develop by Story Mapping

[33mcommit f11c9eeefe6fe66f5ba2a8139174532dc5c842f8[m
Author: Suriya Kenma <60160019@go.buu.ac.th>
Date:   Wed Dec 2 12:10:54 2020 +0700

    Materials - Create Materials Folder

[33mcommit b4e5b0126ccee0d89fe8403c3f3eef287d457053[m
Author: Suriya Kenma <bekzerker@gmai.com>
Date:   Wed Dec 2 11:36:21 2020 +0700

    Project - Change Version to GitLab

[33mcommit 84968d2c9a98bf12f654af1ffb604cd38e680160[m
Author: Suriya Kenma <bekzerker@gmai.com>
Date:   Wed Dec 2 11:27:20 2020 +0700

    Init and add Gitignore
